module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    'transform-inline-environment-variables',
    [
      'module-resolver',
      {
        root: ['.'],
        alias: {
          '@api': './src/api',
          '@hooks': './src/hooks',
          '@utils': './src/utils',
          '@redux': './src/redux',
          '@config': './src/config',
          '@container': './src/container',
          '@components': './src/components',
          '@route': './src/route',
          '@locale': './src/locale',
          '@i18n': './src/i18n',
        },
        extensions: ['.ios.js', '.android.js', '.js', '.json'],
      },
    ],
  ],
};

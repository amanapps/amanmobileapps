import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { createStackNavigator } from '@react-navigation/stack';

import SplashScreen from '@container/Splash';

import { selectData } from '@redux/reducers/misc/setLoginPath';

import AppStack from './AppStack';
import AuthStack from './AuthStack';

const Stack = createStackNavigator();

const Navigation = ({ path }) => {
  let headerShown = false;
  let screen = <Stack.Screen name="Splash" component={SplashScreen} />;

  if (path === 'Auth') {
    screen = <Stack.Screen name="Auth" component={AuthStack} />;
  } else if (path === 'App') {
    headerShown = false;
    screen = <Stack.Screen name="App" component={AppStack} />;
  }

  return (
    <Stack.Navigator initialRouteName="Splash" screenOptions={{ headerShown }}>
      {screen}
    </Stack.Navigator>
  );
};

const mapStateToProps = createStructuredSelector({
  path: selectData,
});

export default connect(mapStateToProps)(Navigation);

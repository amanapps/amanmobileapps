// @flow
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import OTP from '@container/Auth/OTP';
import Login from '@container/Auth/Login';
import Registration from '@container/Auth/Registration';
import ForgotPassword from '@container/Auth/ForgotPassword';

const Stack = createStackNavigator();

const AuthStack = () => (
  <Stack.Navigator
    initialRouteName="Login"
    screenOptions={{ headerShown: false }}>
    <Stack.Screen name="Login" component={Login} />
    <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
    <Stack.Screen name="Registration" component={Registration} />
    <Stack.Screen name="OTP" component={OTP} />
  </Stack.Navigator>
);

export default AuthStack;

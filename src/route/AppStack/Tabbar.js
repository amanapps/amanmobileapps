import React, { useState } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Dimensions,
  View,
  Animated,
  TouchableWithoutFeedback,
} from 'react-native';
import * as shape from 'd3-shape';
import Svg, { Path } from 'react-native-svg';
import { Text, TouchableRipple } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { mediumFont } from '../../config/theme';

const AnimatedSvg = Animated.createAnimatedComponent(Svg);
const { width } = Dimensions.get('window');
const height = 52;
// const tabWidth = width / tabs.length;
const backgroundColor = 'white';

const getPath = tabWidth => {
  const left = shape
    .line()
    .x(d => d.x)
    .y(d => d.y)([
    { x: 0, y: 0 },
    { x: width, y: 0 },
  ]);
  const tab = shape
    .line()
    .x(d => d.x)
    .y(d => d.y)
    .curve(shape.curveBasis)([
    { x: width, y: 0 },
    { x: width + 5, y: 0 },
    { x: width + 10, y: 10 },
    { x: width + 15, y: height },
    { x: width + tabWidth - 15, y: height },
    { x: width + tabWidth - 10, y: 10 },
    { x: width + tabWidth - 5, y: 0 },
    { x: width + tabWidth, y: 0 },
  ]);
  const right = shape
    .line()
    .x(d => d.x)
    .y(d => d.y)([
    { x: width + tabWidth, y: 0 },
    { x: width * 2, y: 0 },
    { x: width * 2, y: height },
    { x: 0, y: height },
    { x: 0, y: 0 },
  ]);
  return `${left} ${tab} ${right}`;
};

class Tabbar extends React.Component {
  constructor(props) {
    super(props);
    const {
      state: { routes, index },
    } = props;
    this.tabWidth = Math.round(width / routes.length);

    this.value = new Animated.Value(this.tabWidth * index);
  }

  render() {
    const translateX = this.value.interpolate({
      inputRange: [0, width],
      outputRange: [-width, 0],
    });

    const d = getPath(this.tabWidth);
    return (
      <>
        <View {...{ height, width }}>
          <AnimatedSvg
            width={width * 2}
            height={height}
            fill="white"
            style={{ transform: [{ translateX }] }}>
            <Path d={d} stroke={backgroundColor} />
          </AnimatedSvg>
          <View style={StyleSheet.absoluteFill}>
            <StaticTabbar
              {...this.props}
              value={this.value}
              tabWidth={this.tabWidth}
            />
          </View>
        </View>
        <SafeAreaView style={styles.container} />
      </>
    );
  }
}

class StaticTabbar extends React.Component {
  constructor(props) {
    super(props);
    const {
      state: { routes, index },
    } = props;
    this.tabs = routes;
    this.values = routes.map((tab, key) => {
      return new Animated.Value(key === index ? 1 : 0);
    });
  }

  onPress = index => {
    const {
      value,
      state: { routes, index: stateIndex },
      navigation,
      tabWidth,
    } = this.props;
    Animated.sequence([
      Animated.parallel(
        this.values.map(v =>
          Animated.timing(v, {
            toValue: 0,
            duration: 100,
            useNativeDriver: true,
          }),
        ),
      ),
      Animated.parallel([
        Animated.spring(value, {
          toValue: tabWidth * index,
          useNativeDriver: true,
        }),
        Animated.spring(this.values[index], {
          toValue: 1,
          useNativeDriver: true,
        }),
      ]),
    ]).start();

    const route = routes[index];
    const isFocused = stateIndex === index;

    const event = navigation.emit({
      type: 'tabPress',
      target: route.key,
    });

    if (!isFocused && !event.defaultPrevented) {
      navigation.navigate(route.name);
    }
  };

  renderTab = (route, index) => {
    const { descriptors, tabWidth, value } = this.props;
    console.log(this.props);
    const { options } = descriptors[route.key];

    const iconName = options.tabBarIcon;

    const cursor = tabWidth * index;
    const opacity = value.interpolate({
      inputRange: [cursor - tabWidth, cursor, cursor + tabWidth],
      outputRange: [1, 0, 1],
      extrapolate: 'clamp',
    });
    const translateY = this.values[index].interpolate({
      inputRange: [0, 1],
      outputRange: [64, 0],
      extrapolate: 'clamp',
    });
    const opacity1 = this.values[index].interpolate({
      inputRange: [0, 1],
      outputRange: [0, 1],
      extrapolate: 'clamp',
    });
    return (
      <React.Fragment key={index}>
        <TouchableWithoutFeedback onPress={() => this.onPress(index)}>
          <Animated.View style={[styles.tab, { opacity }]}>
            {/* <Icon name={iconName} color="black" size={25} /> */}
            {options.tabBarIcon({
              focused: false,
              size: 25,
              color: options.tabBarInactiveTintColor,
            })}
            <Text
              theme={mediumFont}
              style={{ fontSize: 10, lineHeight: 12, color: '#B6B7B7' }}>
              {route.name}
            </Text>
          </Animated.View>
        </TouchableWithoutFeedback>
        <Animated.View
          style={{
            position: 'absolute',
            top: -8,
            left: tabWidth * index,
            width: tabWidth,
            height: 52,
            justifyContent: 'center',
            alignItems: 'center',
            opacity: opacity1,
            transform: [{ translateY }],
          }}>
          <View style={styles.activeIcon}>
            {/* <Icon name={iconName} color="black" size={25} /> */}
            {options.tabBarIcon({
              focused: true,
              size: 25,
              color: options.tabBarActiveTintColor,
            })}
          </View>
        </Animated.View>
      </React.Fragment>
    );
  };

  render() {
    return (
      <View style={styles.container1}>{this.tabs.map(this.renderTab)}</View>
    );
  }
}

export default Tabbar;

const styles = StyleSheet.create({
  container: {
    backgroundColor,
  },
  container1: {
    flexDirection: 'row',
  },
  tab: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 52,
  },
  activeIcon: {
    backgroundColor: 'white',
    width: 40,
    height: 40,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

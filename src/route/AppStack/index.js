// @flow
import React from 'react';
import { Image } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

import Home from '@container/App/Home';
import Tabbar from './Tabbar';
import Profile from '../../container/App/Profile';
import Contact from '../../container/App/Contact';
import PurchaseService from '../../container/App/PurchaseService';

const Icons = {
  Home: 'home_dark',
  Search: 'search_dark',
  Profile: 'user_dark',
  Activity: 'activitiy_dark',
  History: 'history_dark',
};
const Main = props => (
  <Tab.Navigator
    initialRouteName="Home"
    screenOptions={({ route }) => ({
      headerShown: false,
      tabBarIcon: ({ focused, color, size }) => {
        // You can return any component that you like here!
        console.log(size);
        return (
          <Image
            source={{ uri: Icons[route.name] }}
            resizeMode="center"
            style={{
              backgroundColor: 'transparent',
              width: size,
              height: size,
              tintColor: color,
            }}
            tint={color}
          />
        );
      },
      tabBarActiveTintColor: '#000000',
      tabBarInactiveTintColor: '#B6B7B7',
    })}
    tabBar={props => <Tabbar {...props} />}>
    <Tab.Screen name="Home" component={Home} />
    <Tab.Screen name="Search" component={Search} />
    <Tab.Screen name="History" component={History} />
    <Tab.Screen name="Profile" component={Profile} />
    <Tab.Screen name="Activity" component={Activity} />
  </Tab.Navigator>
);

import { i18n } from '@i18n';
import { Avatar } from 'react-native-paper';
import Search from '../../container/App/Search';
import History from '../../container/App/History';
import Activity from '../../container/App/Activity';
import { LargeAds } from '../../container/App/LargeAds';
import UpdateProfile from '../../container/App/UpdateProfile';

const AppStack = () => (
  <Stack.Navigator>
    <Stack.Screen
      name="Main"
      component={Main}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name="PurchaseService"
      component={PurchaseService}
      options={{ title: i18n.t('service_type_label') }}
    />
    <Stack.Screen
      name="LargeAds"
      component={LargeAds}
      options={{ title: i18n.t('general_ads') }}
    />
    <Stack.Screen
      name="Contact"
      component={Contact}
      options={{ title: i18n.t('contact_us') }}
    />
    <Stack.Screen
      name="UpdateProfile"
      component={UpdateProfile}
      initialParams={{ edit: true }}
      options={{ title: i18n.t('edit_profile') }}
    />
  </Stack.Navigator>
);

export default AppStack;

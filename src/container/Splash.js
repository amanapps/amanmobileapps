import { connect } from 'react-redux';
import { Text } from 'react-native-paper';
import { createStructuredSelector } from 'reselect';
import { View, StyleSheet, Animated } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useEffect, useRef, useState, useCallback } from 'react';

import Logo from '@components/Logo';
import ESCOLogo from '@components/ESCOLogo';

import { i18n } from '@i18n';
import { setUserID } from '@redux/reducers/auth/setUserID';
import { setLoginPath } from '@redux/reducers/misc/setLoginPath';
import { regularFont } from '../config/theme';

import constants from '@config/constant';

import {
  getUserDetails,
  selectLoadingStatus as userLoading,
} from '@redux/reducers/user/getDetails';

import { setAuthToken } from '../redux/reducers/auth/setAuthToken';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  loader: {
    flex: 0,
    marginVertical: 16,
  },
  bold: {
    fontFamily: 'Poppins-Bold',
  },
  logo: {
    width: 244,
    height: 168,
  },
  platform: {
    marginTop: 36,
    fontSize: 36,
  },
  desc: {
    fontSize: 17,
    lineHeight: 23,
    paddingHorizontal: '15%',
    textAlign: 'center',
  },
  partnershipContainer: {
    paddingHorizontal: '15%',
    marginTop: 16,
    textAlign: 'center',
  },
  partnership: {
    color: '#171717',
    fontSize: 16,
    lineHeight: 20,
  },
  companyContainer: {
    alignItems: 'center',
    position: 'absolute',
    bottom: 100,
  },
  company: {
    fontSize: 22,
  },
  escoContainer: {
    alignItems: 'center',
    position: 'absolute',
    bottom: 8,
  },
  esco: {
    // alignSelf: 'flex-end',
  },
});

const SplashScreen = ({
  setToken,
  navigation,
  SetLoginPath,
  SetUserID,
  getProfile,
}) => {
  const opacity = useRef(new Animated.Value(0)).current;
  const scale = useRef(new Animated.Value(0)).current;

  useEffect(() => {
    Animated.parallel([
      Animated.timing(opacity, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: true,
      }),
      Animated.timing(scale, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: true,
      }),
    ]).start(async () => {
      const token = await AsyncStorage.getItem(constants.AUTH_TOKEN);

      if (token) {
        setToken(token);
        getProfile();
        SetLoginPath('App');
      } else {
        SetLoginPath('Auth');
      }
    });
  }, [scale, opacity, setToken, SetLoginPath, getProfile]);

  return (
    <View style={styles.container}>
      <Text style={[styles.bold, styles.platform]}>
        {i18n.t('splash_heading')}
      </Text>
      {/* <Animated.View style={{ opacity, transform: [{ scale }] }}> */}
      <Logo style={styles.logo} />
      {/* </Animated.View> */}
      <Text style={[styles.bold, styles.desc]}>
        {i18n.t('splash_description')}
      </Text>
      <Text
        theme={regularFont}
        style={[styles.partnership, styles.partnershipContainer]}>
        {i18n.t('splash_partnership')}
      </Text>

      <View style={styles.companyContainer}>
        <Text theme={regularFont} style={styles.partnership}>
          {i18n.t('splash_implementation')}
        </Text>
        <Text style={[styles.bold, styles.company]}>
          {i18n.t('splash_company')}
        </Text>
      </View>

      <View style={styles.escoContainer}>
        <Text theme={regularFont} style={styles.partnership}>
          {i18n.t('splash_powered_by')}
        </Text>
        <ESCOLogo style={styles.esco} />
      </View>
    </View>
  );
};

const mapStateToProps = createStructuredSelector({});

const mapDispatchToProps = {
  SetLoginPath: setLoginPath,
  SetUserID: setUserID,
  getProfile: getUserDetails,
  setToken: setAuthToken,
};

export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen);

import { connect } from 'react-redux';
import {
  Appbar,
  Avatar,
  Card,
  Colors,
  Searchbar,
  Text,
} from 'react-native-paper';
import { createStructuredSelector } from 'reselect';
import { FlatList, ScrollView } from 'react-native-gesture-handler';
import React, { useCallback, useEffect, useState } from 'react';
import { Dimensions, View, StyleSheet, ImageBackground } from 'react-native';

import { i18n } from '@i18n';

import Loader from '@components/Loader';
import Author from '@components/Author';
import AppContainer from '@components/AppContainer';

import {
  getMemberSearch,
  selectData,
  selectErrorData,
  selectErrorStatus,
  selectIsSearched,
  selectLoadingStatus,
} from '../../redux/reducers/committe/search';

// import { selectData as selectUserId } from '@store/reducers/auth/setUserID';
import { showLoader, hideLoader } from '@redux/reducers/rootLoaderReducer';

const SearchResult = props => {
  // {
  //     "badge_no": "0001",
  //         "book_no": "B0001",
  //             "committee_member_name": "ABC",
  //                 "expiry_date": "2021-12-01",
  //                     "is_expired": false
  // }
  const { badge_no, book_no, committee_member_name, expiry_date, is_expired } =
    props;
  const flag = is_expired ? i18n.t('search_expired') : i18n.t('search_active');
  const style = is_expired ? styles.inaActiveFlag : styles.activeflag;
  return (
    <Card
      theme={{ roundness: 36 }}
      elevation={12}
      style={{ marginVertical: 16 }}>
      <Card.Content style={{ margin: 12 }}>
        <View style={styles.row}>
          <View style={{ flex: 1 }}>
            <Text style={styles.title}>{i18n.t('committee_member_name')}</Text>
            <Text style={styles.value}>{committee_member_name}</Text>
          </View>
          <View style={styles.flagContainer}>
            <Text style={style}>{flag}</Text>
          </View>
        </View>
        <View style={[styles.row, styles.margin]}>
          <View style={{ flex: 1 }}>
            <Text style={styles.title}>{i18n.t('badge')}</Text>
            <Text style={styles.value}>{badge_no}</Text>
          </View>
          <View style={{ flex: 1 }}>
            <Text style={styles.title}>{i18n.t('book')}</Text>
            <Text style={styles.value}>{book_no}</Text>
          </View>
        </View>
        <View>
          <Text style={styles.title}>{i18n.t('expiry')}</Text>
          <Text style={styles.value}>{expiry_date}</Text>
        </View>
      </Card.Content>
    </Card>
  );
};
const Search = ({ navigation, ...props }) => {
  const { ShowModal, HideModal, data, loading, memberSearch, searched } = props;
  const [searchQuery, setSearchQuery] = React.useState('');

  const onChangeSearch = query => setSearchQuery(query);

  const _renderItem = useCallback(({ item }) => <SearchResult {...item} />, []);

  const onSubmit = useCallback(() => {
    memberSearch(searchQuery);
  }, [memberSearch, searchQuery]);

  // useEffect(() => {
  //   // set toggleDrawer function in params
  //   if (drawerProps.show) {
  //     navigation.toggleDrawer();
  //     setTimeout(() => {
  //       Hidedrawer();
  //     }, 1000);
  //   }
  // }, [Hidedrawer, drawerProps.show, navigation]);

  useEffect(() => {
    if (loading) {
      ShowModal();
    } else {
      HideModal();
    }
  }, [HideModal, ShowModal, loading]);

  return (
    <ScrollView contentContainerStyle={styles.contentContainerStyle}>
      {/* <Appbar.Header
        theme={{ colors: { primary: '#fff' } }}
        style={{ elevation: 0 }}>
        <Appbar.BackAction />
        <Appbar.Content
          title="Search"
          style={{ fontFamily: 'Poppins-Bold', fontSize: 24 }}
        />
      </Appbar.Header> */}
      <AppContainer style={styles.wrapper}>
        <View style={{ marginHorizontal: 16 }}>
          <Author width="100%" />
        </View>
        <Searchbar
          placeholder="Search"
          onChangeText={onChangeSearch}
          onSubmitEditing={onSubmit}
          value={searchQuery}
          theme={{ colors: { text: '#7C7D7E' } }}
          style={styles.searchBar}
        />

        {!loading && data.length && (
          <FlatList
            data={data}
            renderItem={_renderItem}
            contentContainerStyle={styles.flatListContainer}
            keyExtractor={item => item.id}
          />
        )}

        {!loading && !data.length && !searchQuery && null}
        {!loading && !data.length && searched ? (
          <Card
            theme={{ roundness: 36 }}
            elevation={12}
            style={{ marginVertical: 16, marginHorizontal: 16 }}>
            <Card.Content style={{ margin: 12 }}>
              <View
                style={{ flex: 1, alignSelf: 'center', paddingVertical: 16 }}>
                <Text
                  style={[
                    styles.title,
                    { color: Colors.red400, textAlign: 'center' },
                  ]}>
                  {i18n.t('search_not_found')}
                </Text>
              </View>
            </Card.Content>
          </Card>
        ) : null}
      </AppContainer>
    </ScrollView>
  );
};

const mapStateToProps = createStructuredSelector({
  data: selectData,
  errorData: selectErrorData,
  loading: selectLoadingStatus,
  searched: selectIsSearched,
});

const mapDispatchToProps = {
  memberSearch: getMemberSearch,
  ShowModal: showLoader,
  HideModal: hideLoader,
};

export default connect(mapStateToProps, mapDispatchToProps)(Search);

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: '#fff',
  },
  contentContainerStyle: {
    flexGrow: 1,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
  },
  margin: {
    marginVertical: 10,
  },
  searchBar: {
    borderRadius: 28,
    backgroundColor: '#F2F2F2',
    elevation: 0,
    marginVertical: 16,
    marginHorizontal: 16,
  },
  title: {
    color: '#4A4B4D',
    fontSize: 14,
    lineHeight: 21,
    fontFamily: 'Poppins-Bold',
  },
  value: {
    color: '#4A4B4D',
    fontSize: 14,
    lineHeight: 21,
    fontFamily: 'Poppins-Light',
  },
  flagContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  inaActiveFlag: {
    color: '#FFFFFF',
    fontSize: 12,
    lineHeight: 18,
    paddingVertical: 4,
    paddingHorizontal: 8,
    backgroundColor: '#F1310A',
    borderRadius: 12,
    fontFamily: 'Poppins-Bold',
  },
  activeflag: {
    color: '#FFFFFF',
    fontSize: 12,
    lineHeight: 18,
    paddingVertical: 4,
    paddingHorizontal: 8,
    backgroundColor: '#44CE1B',
    borderRadius: 12,
    fontFamily: 'Poppins-Bold',
  },
  flatListContainer: {
    paddingHorizontal: 16,
  },
});

import { connect } from 'react-redux';
import {
  TextInput,
  Avatar,
  Card,
  Searchbar,
  Text,
  Button,
} from 'react-native-paper';
import DropDown from 'react-native-paper-dropdown';

import { createStructuredSelector } from 'reselect';
import { FlatList, ScrollView } from 'react-native-gesture-handler';
import React, { useCallback, useEffect, useState } from 'react';
import { Dimensions, View, StyleSheet, ImageBackground } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { i18n } from '@i18n';
import constants from '@config/constant';

import Loader from '@components/Loader';
import Author from '@components/Author';
import AppContainer from '@components/AppContainer';

import {
  selectData,
  getUserDetails,
} from '../../redux/reducers/user/getDetails';
import {
  updateProfile,
  selectLoadingStatus,
  selectErrorData,
  selectData as updatedata,
  reset,
} from '../../redux/reducers/user/updateProfile';

import { logoutSuccess } from '../../redux/reducers/misc/logout';

import { setLoginPath } from '@redux/reducers/misc/setLoginPath';

import {
  selectData as stateData,
  getCountryStates,
  selectErrorData as stateError,
  selectErrorStatus,
  selectLoadingStatus as stateLoading,
} from '@redux/reducers/misc/getCountryStates';

import {
  selectData as businessTypeData,
  getBusinessType,
  selectErrorData as businessTypeErrorData,
  selectErrorStatus as businessTypeErrorStatus,
  selectLoadingStatus as businessTypeLoading,
} from '@redux/reducers/misc/getBussinessType';

import { showLoader, hideLoader } from '@redux/reducers/rootLoaderReducer';

const UpdateProfile = ({ navigation, route, ...props }) => {
  const {
    ShowModal,
    HideModal,
    profile: pDAta,

    businessTypeData: bData,
    businessTypeLoading: bLoading,
    GetBusinessType,

    GetCountryStates,
    stateData,
    stateLoading,

    loading,
    data,
    error,
    update,
    Reset,

    fetchProfile,

    logout,
  } = props;
  const { params: { edit = false } = {} } = route;

  const [profile, setProfile] = useState({});

  const [showStateDropDown, setShowStateDropDown] = useState(false);
  const [showBusinessDropDown, setShowBusinessDropDown] = useState(false);

  useEffect(() => {
    GetCountryStates('106');
    GetBusinessType();
  }, [GetCountryStates, GetBusinessType]);

  useEffect(() => {
    if (data) {
      fetchProfile();
      navigation.pop();
    }
  }, [data, navigation, fetchProfile]);

  const onPress = useCallback(() => {
    console.log(profile);
    update(profile);
  }, [profile, update]);

  const onSetFormData = useCallback(
    field_name => text => {
      setProfile({
        ...profile,
        [field_name]: text,
      });
    },
    [setProfile, profile],
  );

  useEffect(() => {
    const { image_url, state_id, business_type_name, ...rest } = pDAta;
    setProfile(rest);

    return Reset;
  }, [Reset, pDAta]);

  useEffect(() => {
    if (loading) {
      ShowModal();
    } else {
      HideModal();
    }
  }, [HideModal, ShowModal, loading]);

  useEffect(() => {
    if (error && Object.keys(error).length) {
      alert(JSON.stringify(error));
    }
  }, [error]);

  if (stateLoading || bLoading) {
    return <Loader />;
  }

  // useEffect(() => GetUserDetails(currentUser), [GetUserDetails, currentUser]);

  const disabled = !edit;
  const roundness = 16;
  const mode = 'outlined';
  const theme = { roundness: 16, colors: { text: '#000' } };

  return (
    <ScrollView contentContainerStyle={styles.contentContainerStyle}>
      <AppContainer style={styles.wrapper}>
        <View style={{ marginHorizontal: 16 }}>
          <TextInput
            theme={{ roundness, colors: { text: '#000' } }}
            mode={mode}
            disabled={disabled}
            label={i18n.t('Name')}
            value={profile.name}
            onChangeText={onSetFormData('name')}
            style={styles.input}
          />

          <DropDown
            mode="mode"
            theme={theme}
            inputProps={{
              style: styles.input,
              underlineColor: 'transparent',
              disabled,
              mode,
              label: i18n.t('registration_business_type_label'),
            }}
            visible={showBusinessDropDown}
            showDropDown={() => setShowBusinessDropDown(true)}
            onDismiss={() => setShowBusinessDropDown(false)}
            value={profile.business_type_id}
            setValue={onSetFormData('business_type_id')}
            list={bData}
          />

          <DropDown
            mode="mode"
            theme={theme}
            inputProps={{
              style: styles.input,
              underlineColor: 'transparent',
              disabled,
              mode,
              label: i18n.t('registration_state_label'),
            }}
            visible={showStateDropDown}
            showDropDown={() => setShowStateDropDown(true)}
            onDismiss={() => setShowStateDropDown(false)}
            value={profile.state_code}
            setValue={onSetFormData('state_code')}
            list={stateData}
          />
          {/* <TextInput
            theme={{ roundness, colors: { text: '#000' } }}
            mode={mode}
            disabled={disabled}
            label={i18n.t('Union')}
            value={profile.union}
            style={styles.input}
          /> */}
          <TextInput
            theme={{ roundness, colors: { text: '#000' } }}
            mode={mode}
            disabled={disabled}
            label={i18n.t('City')}
            onChangeText={onSetFormData('city')}
            value={profile.city}
            style={styles.input}
          />
          <TextInput
            theme={{ roundness, colors: { text: '#000' } }}
            mode={mode}
            disabled={disabled}
            label={i18n.t('street')}
            onChangeText={onSetFormData('street')}
            value={profile.street}
            style={styles.input}
          />
          <TextInput
            theme={{ roundness, colors: { text: '#000' } }}
            mode={mode}
            disabled={disabled}
            value={profile.street2}
            label={i18n.t('street_2')}
            onChangeText={onSetFormData('street2')}
            style={styles.input}
          />
          <TextInput
            theme={{ roundness, colors: { text: '#000' } }}
            mode={mode}
            disabled={disabled}
            value={profile.mobile}
            label={i18n.t('business_phone')}
            onChangeText={onSetFormData('mobile')}
            style={styles.input}
          />
          <TextInput
            theme={{ roundness, colors: { text: '#000' } }}
            mode={mode}
            disabled={disabled}
            label={i18n.t('Website')}
            onChangeText={onSetFormData('website')}
            value={profile.website}
            style={styles.input}
          />
          <TextInput
            theme={{ roundness, colors: { text: '#000' } }}
            mode={mode}
            disabled={disabled}
            label={i18n.t('owner_name')}
            onChangeText={onSetFormData('owner_name')}
            value={profile.owner_name}
            style={styles.input}
          />
          <TextInput
            theme={{ roundness, colors: { text: '#000' } }}
            mode={mode}
            disabled={disabled}
            maxLength={10}
            keyboardType="number-pad"
            label={i18n.t('owner_mobile')}
            onChangeText={onSetFormData('owner_mobile')}
            value={profile.owner_mobile}
            style={[styles.input, { marginBottom: 24 }]}
          />

          <Button
            mode="contained"
            onPress={onPress}
            style={styles.last}
            theme={{
              roundness: 36,
              colors: { primary: '#338e86' },
              fonts: { medium: 'Poppins-Bold' },
            }}
            loading={loading}
            labelStyle={styles.buttonLabel}
            disabled={loading}>
            {i18n.t('update')}
          </Button>
        </View>
      </AppContainer>
    </ScrollView>
  );
};

const mapStateToProps = createStructuredSelector({
  // drawerProps: selectBottomSheetProps,
  // currentUser: selectUserId,
  // userData: selectUserDetails,
  // userErrorData: selectUserErrorData,
  // userLoading: selectUserLoadingStatus,
  profile: selectData,
  error: selectErrorData,
  data: updatedata,
  loading: selectLoadingStatus,

  stateData,
  stateError,
  stateLoading,

  businessTypeLoading,
  businessTypeErrorData,
  businessTypeData,
});

const mapDispatchToProps = {
  // Hidedrawer: hidedrawer,
  // GetUserDetails: getUserDetails,
  Reset: reset,
  ShowModal: showLoader,
  HideModal: hideLoader,
  update: updateProfile,
  SetLoginPath: setLoginPath,
  GetCountryStates: getCountryStates,
  GetBusinessType: getBusinessType,
  fetchProfile: getUserDetails,
  logout: logoutSuccess,
};

export default connect(mapStateToProps, mapDispatchToProps)(UpdateProfile);

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: '#fff',
  },
  contentContainerStyle: {
    flexGrow: 1,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
  },
  margin: {
    marginVertical: 10,
  },
  input: {
    marginBottom: 8,
    borderRadius: 16,
    fontFamily: 'Poppins-Medium',
  },
  last: { marginBottom: 50 },
});

import React from 'react';
import { View, Image, StyleSheet, Dimensions } from 'react-native';

const SLIDER_WIDTH = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.88);
const ITEM_HEIGHT = Math.round((ITEM_WIDTH * 3) / 4);

export const LargeAds = ({ route, navigation, ...rest }) => {
  const { item } = route.params;

  return (
    <View style={styles.container}>
      <Image source={{ uri: item.image_url }} style={styles.itemContainer} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  itemContainer: {
    width: ITEM_WIDTH,
    height: ITEM_HEIGHT,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#BEBDBD',
    borderRadius: 10,
    overflow: 'hidden',
  },
});

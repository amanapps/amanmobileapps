import React, { useCallback, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { ScrollView, StyleSheet, View } from 'react-native';

import { i18n } from '@i18n';
import AppContainer from '@components/AppContainer';

import {
  registerUser,
  selectData,
  selectErrorData,
  selectErrorStatus,
  selectLoadingStatus,
} from '@redux/reducers/auth/registration';
import { showModal } from '@redux/reducers/modals';
import { setLoginPath } from '@redux/reducers/misc/setLoginPath';

import ServiceType from './ServiceType';
import CarrierInformation from './CarrierInformation';
import MembershipPackage from './MembershipPackage';
import HealthDeclarationForm from './HealthDeclarationForm';

const PurchaseService = props => {
  const { navigation } = props;

  const [page, setPage] = useState(0);
  const [maps] = useState([
    { type: ServiceType, title: i18n.t('service_type_label') },
    { type: CarrierInformation, title: i18n.t('carrier_info_label') },
    { type: MembershipPackage, title: i18n.t('membership_package_label') },
    { type: HealthDeclarationForm, title: i18n.t('health_declaration_label') },
  ]);

  useEffect(() => {
    const { title } = maps[page];
    navigation.setOptions({ title });
  }, [page, navigation, maps]);

  const { type: Comp } = maps[page];
  return (
    <ScrollView contentContainerStyle={styles.scroll}>
      <AppContainer style={styles.container}>
        <Comp onPress={() => setPage(page + 1)} />
      </AppContainer>
    </ScrollView>
  );
};

const mapStateToProps = createStructuredSelector({
  data: selectData,
  errorData: selectErrorData,
  loading: selectLoadingStatus,
  // sentData: sendOTPSelectData,
  // sentError: sendOTPSelectErrorData,
  // sentLoading: sendOTPSelectLoadingStatus,
});

const mapDispatchToPrpos = {
  Register: registerUser,
  Show: showModal,
  SetLoginPath: setLoginPath,
};

export default connect(mapStateToProps, mapDispatchToPrpos)(PurchaseService);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: 16,
  },
  row: {
    flexDirection: 'row',
  },
  scroll: {
    flexGrow: 1,
  },
  logo: {
    width: 200,
    alignSelf: 'center',
  },
  textInput: {
    marginBottom: 12,
  },
  textLabel: {
    fontSize: 16,
  },
  radioOption: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  escoContainer: {
    flex: 1,
    marginVertical: 16,
    justifyContent: 'center',
    alignItems: 'center',
  },
  escoLogo: {},
});

import React, { useCallback, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { ScrollView, StyleSheet, View } from 'react-native';
import { Dropdown } from 'react-native-material-dropdown-v2-fixed';

import {
  Caption,
  Button,
  Text,
  Title,
  RadioButton,
  Subheading,
} from 'react-native-paper';

import { i18n } from '@i18n';
import AppContainer from '@components/AppContainer';

import {
  selectData,
  getServiceTypes,
  selectErrorData,
  selectErrorStatus,
  selectLoadingStatus,
} from '@redux/reducers/purchaseService/getServiceType';

import { selectPostData, setPostData } from '@redux/reducers/auth/registration';

const question = {
  1: '1- امراض القلب والشرايين (الضغط – شرايين القلب – عضل القلب – جلطة في الدماغ – نزيف في الدماغ – سائر امراض القلب والشرايين)',
  10: '10- امراض خبيثة (اورام خبيثة وامراض خبيثة في الدم)',
  11: '11- الامراض المنتقلة جنسيا ومرض او فيروس الايدز',
  12: '12- امراض اخرى. حوادث او عمليات تنظير او فحوصات تشخيصية سابقة او مستقبلية على علم مسبق بها',
  13: '13- هل تتناول اي دواء او تتابع اي علاج',
  14: '14- للنساء: هل انت حامل في الوقت الحاضر',
  15: '15- الامراض والعاهات الخلقية',
  16: '16- الامراض النفسية (كأبة – توتر)',
  17: '17- هل تعاني من عوارض متعلقة بالأمراض المذكورة اعاله (اوجاع ظهر – اوجاع صدر – اوجاع مفاصل)',
  2: '2- امراض الجهاز التنفسي ماعدا السرطان (الربو –انسداد القصبات الهوائية – تشمع الرئة – امراض أخرى)',
  3: '3- امراض الجهاز الهضمي ماعدا السرطان (قولون – التهاب الامعاء – جيوب في الامعاء – امراض المرارة – الكبد – البنكرياس)',
  4: '4- امراض الكلى والمسالك البولية (حصوة – التهاب – قصور – تكيس)',
  5: '5- امراض العضم ماعدا السرطان او الاطراف المزروعة',
  6: '6- امراض الجهاز العصبي ماعدا السرطان (شلل الاطفال – انهيار عصبي – تليف لويحي – الصرع ...)',
  7: '7- السكري او امراض الغدد ماعدا السرطان',
  8: '8- امراض الاذن، الانف والحنجرة او العين ماعدا السرطان',
  9: '9- امراض الدم ماعدا الامراض الخبيثة (فقر الدم)',
};

const HealthDeclaration = props => {
  const { data, GetServiceTypes, loading, onPress, SetPostData, postData } =
    props;

  //   useEffect(() => {
  //     GetServiceTypes();
  //   }, [GetServiceTypes]);

  return (
    <View style={styles.container}>
      {/* <Title>{i18n.t('health_declaration_label')}</Title> */}

      <Caption style={{ marginBottom: 16 }}>
        {i18n.t('declaration_caption')}
      </Caption>

      {Object.keys(question).map((key, index) => {
        console.log(question[key]);
        return (
          <View key={index}>
            <Text style={{ color: '#000' }}>{question[key]}</Text>

            <RadioButton.Group onValueChange={newValue => {}} value="no">
              <View style={styles.row}>
                <View style={styles.row}>
                  <RadioButton />
                  <Text>{i18n.t('yes_label')}</Text>
                </View>
                <View style={styles.row}>
                  <RadioButton />
                  <Text>{i18n.t('no_label')}</Text>
                </View>
              </View>
            </RadioButton.Group>
          </View>
        );
      })}

      <Button mode="contained" onPress={onPress}>
        {i18n.t('next_label')}
      </Button>
    </View>
  );
};

const mapStateToProps = createStructuredSelector({
  data: selectData,
  errorData: selectErrorData,
  loading: selectLoadingStatus,
  postData: selectPostData,
});

const mapDispatchToPrpos = {
  GetServiceTypes: getServiceTypes,
  SetPostData: setPostData,
};

export default connect(mapStateToProps, mapDispatchToPrpos)(HealthDeclaration);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: 8,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  pickerStyle: {
    // flex: 1,
    borderRadius: 4,
    borderWidth: 1.5,
    marginBottom: 12,
  },
  pickerInput: {
    marginBottom: 0,
    backgroundColor: '#fff',
  },
});

import React, { useCallback, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { ScrollView, StyleSheet, View } from 'react-native';
import { Dropdown } from 'react-native-material-dropdown-v2-fixed';

import {
  Caption,
  Button,
  TextInput,
  Title,
  RadioButton,
} from 'react-native-paper';

import { i18n } from '@i18n';
import AppContainer from '@components/AppContainer';

import {
  selectData,
  getServiceTypes,
  selectErrorData,
  selectErrorStatus,
  selectLoadingStatus,
} from '@redux/reducers/purchaseService/getServiceType';

import { selectPostData, setPostData } from '@redux/reducers/auth/registration';

const reg_types = [
  {
    id: 14,
    value: 'Silver (With Bank) (70000 IQD)',
  },
  {
    id: 13,
    value: 'Golden (With Bank) (110000 IQD)',
  },
  {
    id: 15,
    value: 'Daimond (With Bank) (160000 IQD)',
  },
  {
    id: 22,
    value: 'Rimas Health (210000 IQD)',
  },
  {
    id: 16,
    value: 'الباقة البلاتينية - VIP (With Bank) (360000 IQD)',
  },
];

const unions_types = [
  {
    id: 2,
    value: 'مصرف التنمية الدولي',
  },
];

const MembershipPackage = props => {
  const { data, GetServiceTypes, loading, onPress, SetPostData, postData } =
    props;

  useEffect(() => {
    GetServiceTypes();
  }, [GetServiceTypes]);

  return (
    <View style={styles.container}>
      <Title>{i18n.t('membership_package_label')}</Title>

      <Caption style={{ marginBottom: 16 }}>
        {i18n.t('service_type_caption')}
      </Caption>

      <Dropdown
        itemCount={10}
        data={reg_types}
        label={i18n.t('member_membership_label')}
        underlineColor="transparent"
        renderAccessory={() => <TextInput.Icon name="chevron-down" />}
        containerStyle={styles.pickerStyle}
        style={styles.pickerInput}
      />

      <Dropdown
        itemCount={10}
        data={unions_types}
        label={i18n.t('member_bank_label')}
        underlineColor="transparent"
        renderAccessory={() => <TextInput.Icon name="chevron-down" />}
        containerStyle={styles.pickerStyle}
        style={styles.pickerInput}
      />

      <Button mode="contained" onPress={onPress}>
        {i18n.t('next_label')}
      </Button>
    </View>
  );
};

const mapStateToProps = createStructuredSelector({
  data: selectData,
  errorData: selectErrorData,
  loading: selectLoadingStatus,
  postData: selectPostData,
});

const mapDispatchToPrpos = {
  GetServiceTypes: getServiceTypes,
  SetPostData: setPostData,
};

export default connect(mapStateToProps, mapDispatchToPrpos)(MembershipPackage);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: 8,
  },

  pickerStyle: {
    // flex: 1,
    borderRadius: 4,
    borderWidth: 1.5,
    marginBottom: 12,
  },
  pickerInput: {
    marginBottom: 0,
    backgroundColor: '#fff',
  },
});

import React, { useCallback, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { ScrollView, StyleSheet, View } from 'react-native';
import { Dropdown } from 'react-native-material-dropdown-v2-fixed';

import {
  Caption,
  Button,
  TextInput,
  Title,
  RadioButton,
} from 'react-native-paper';

import { i18n } from '@i18n';
import AppContainer from '@components/AppContainer';

import {
  selectData,
  getServiceTypes,
  selectErrorData,
  selectErrorStatus,
  selectLoadingStatus,
} from '@redux/reducers/purchaseService/getServiceType';

import { selectPostData, setPostData } from '@redux/reducers/auth/registration';

const reg_types = [
  {
    english_name: 'Union member',
    id: 5,
    value: 'عضو نقابي',
  },
  {
    english_name: 'Employee',
    id: 6,
    value: 'منتسب',
  },
  {
    english_name: 'Administrative Committee',
    id: 7,
    value: 'هيئات ادارية',
  },
  {
    english_name: 'Aman',
    id: 8,
    value: 'مشترك خدمات أمان',
  },
];

const unions_types = [
  {
    english_name: '',
    id: 131,
    value: 'الاتحاد العام لنقابات العمال في العراق',
  },
  {
    english_name: '',
    id: 133,
    value: 'نقابة عمال الكهرباء',
  },
  {
    english_name: '',
    id: 134,
    value: 'النقابة العامة لعمال الغزل والنسيج',
  },
  {
    english_name: '',
    id: 135,
    value: 'اتحاد نقابات العمال في بابل',
  },
  {
    english_name: '.',
    id: 136,
    value: 'النقابة العامة لعمال البناء والاخشاب',
  },
  {
    english_name: 'Babylon Transport Syndicate',
    id: 137,
    value: 'نقابة النقل والاتصالات في بابل',
  },
  {
    english_name: '',
    id: 138,
    value: 'نقابة الخدمات الاجتماعية والسياحية في بابل',
  },
  {
    english_name: 'Federation of Trade Unions in Basra',
    id: 139,
    value: 'اتحاد نقابات العمال في البصرة',
  },
  {
    english_name:
      'The headquarters of the General Federation of Trade Unions in Iraq',
    id: 140,
    value: 'ممثلية الاتحاد العام لنقابات العمال في العراق',
  },
  {
    english_name: '',
    id: 141,
    value: 'النقابة العامة للخدمات الاجتماعية والسياحية',
  },
  {
    english_name: '',
    id: 142,
    value: 'النقابة العامة للزراعين و تصنيع مواد الغذائية',
  },
  {
    english_name: '',
    id: 143,
    value: 'نقابة الغزل والنسيج في بابل',
  },
  {
    english_name: '',
    id: 144,
    value: 'نقابة البناء والاخشاب في بابل',
  },
  {
    english_name: '',
    id: 145,
    value: 'النقابة العامة لعمال النقل والاتصالات',
  },
  {
    english_name: '',
    id: 146,
    value: 'النقابة العامة للمتقاعدين',
  },
  {
    english_name: '',
    id: 147,
    value: 'نقابة الزراعين و تصنيع مواد الغذائية في بابل',
  },
  {
    english_name: '',
    id: 148,
    value: 'نقابة عمال الميكانيك في بابل',
  },
  {
    english_name: 'Syndicate of Barbers',
    id: 149,
    value: 'نقابة الحلاقين والمزينين في بابل',
  },
  {
    english_name: 'General Syndicate of Oil and Gas Workers',
    id: 151,
    value: 'النقابة العامة للعاملين في قطاع النفط والغاز',
  },
  {
    english_name:
      'Syndicate of Complementary Medicine and Medicinal Herbs in Babylon',
    id: 153,
    value: 'نقابة الطب التكميلي والاعشاب الطبية في بابل',
  },
  {
    english_name: 'Public',
    id: 155,
    value: 'خدمات أمان',
  },
];

const carrier_type = [
  {
    english_name: '',
    id: 24,
    value: 'حداد',
  },
  {
    english_name: 'Electric',
    id: 26,
    value: 'كهربائي',
  },
  {
    english_name: 'Aman',
    id: 27,
    value: 'Aman',
  },
  {
    english_name: 'tailor',
    id: 28,
    value: 'خياط',
  },
  {
    english_name: '',
    id: 30,
    value: 'حياكة السجاد',
  },
  {
    english_name: '',
    id: 31,
    value: 'هيئات ادارية',
  },
  {
    english_name: '',
    id: 32,
    value: 'رئيس اللجنة النقابية',
  },
  {
    english_name: 'Administration',
    id: 33,
    value: 'اداري',
  },
  {
    english_name: '',
    id: 34,
    value: 'نائب رئيس النقابة',
  },
  {
    english_name: 'Union',
    id: 35,
    value: 'نقابي',
  },
  {
    english_name: '',
    id: 36,
    value: 'IT استشاري',
  },
  {
    english_name: '',
    id: 38,
    value: 'عضو لجنه',
  },
  {
    english_name: '',
    id: 39,
    value: 'عضو نقابي',
  },
  {
    english_name: '',
    id: 40,
    value: 'عضو',
  },
  {
    english_name: '',
    id: 41,
    value: 'فنان',
  },
  {
    english_name: '',
    id: 42,
    value: 'عامل',
  },
  {
    english_name: '',
    id: 43,
    value: 'تربية',
  },
  {
    english_name: '',
    id: 44,
    value: 'دواجن',
  },
  {
    english_name: 'taxi driver',
    id: 45,
    value: 'سائق أجرة',
  },
  {
    english_name: 'Support & Maintenance',
    id: 46,
    value: 'صيانة',
  },
  {
    english_name: 'Carpenter',
    id: 47,
    value: 'نجار',
  },
  {
    english_name: 'Dry Clean',
    id: 48,
    value: 'كي وغسل الملابس',
  },
  {
    english_name: 'representative',
    id: 49,
    value: 'مندوب',
  },
  {
    english_name: 'programmer',
    id: 50,
    value: 'مبرمج',
  },
  {
    english_name: '',
    id: 51,
    value: 'صاحب شركة',
  },
  {
    english_name: 'worker',
    id: 52,
    value: 'بناء',
  },
  {
    english_name: '',
    id: 53,
    value: 'عامل ندافة',
  },
  {
    english_name: 'Barber',
    id: 54,
    value: 'كوافير',
  },
  {
    english_name: 'Pharmacy Assistant',
    id: 55,
    value: 'معاون صيدلي',
  },
  {
    english_name: 'Skilled Laber',
    id: 56,
    value: 'عامل حرفي',
  },
  {
    english_name: 'Private Sector',
    id: 57,
    value: 'عمل خاص',
  },
  {
    english_name: 'Engineer',
    id: 58,
    value: 'مهندس',
  },
  {
    english_name: 'Model',
    id: 59,
    value: 'مودل',
  },
  {
    english_name: 'Restaurant and hotel services',
    id: 61,
    value: 'خدمات مطاعم وفنادق',
  },
  {
    english_name: 'Gas station services',
    id: 62,
    value: 'خدمات محطات الوقود',
  },
  {
    english_name: 'Driver',
    id: 66,
    value: 'سائق',
  },
  {
    english_name: 'Complementary medicine and herbs',
    id: 67,
    value: 'طب تكميلي وأعشاب',
  },
  {
    english_name: 'shaving',
    id: 68,
    value: 'حلاقة',
  },
];

const profession = [
  {
    english_name: "Women's haircut",
    id: 297,
    value: 'حلاقة  نسائية',
  },
];

const CarrierInformation = props => {
  const { data, GetServiceTypes, loading, onPress, SetPostData, postData } =
    props;

  useEffect(() => {
    GetServiceTypes();
  }, [GetServiceTypes]);

  return (
    <View style={styles.container}>
      <Title>{i18n.t('carrier_info_label')}</Title>

      <Caption style={{ marginBottom: 16 }}>
        {i18n.t('service_type_caption')}
      </Caption>

      <Dropdown
        itemCount={10}
        data={reg_types}
        label={i18n.t('carrier_registration_label')}
        underlineColor="transparent"
        renderAccessory={() => <TextInput.Icon name="chevron-down" />}
        containerStyle={styles.pickerStyle}
        style={styles.pickerInput}
      />

      <Dropdown
        itemCount={10}
        data={unions_types}
        label={i18n.t('carrier_union_label')}
        underlineColor="transparent"
        renderAccessory={() => <TextInput.Icon name="chevron-down" />}
        containerStyle={styles.pickerStyle}
        style={styles.pickerInput}
      />

      <Dropdown
        itemCount={10}
        data={carrier_type}
        label={i18n.t('carrier_carrier_label')}
        underlineColor="transparent"
        renderAccessory={() => <TextInput.Icon name="chevron-down" />}
        containerStyle={styles.pickerStyle}
        style={styles.pickerInput}
      />

      <Dropdown
        itemCount={10}
        data={profession}
        label={i18n.t('carrier_profession_label')}
        underlineColor="transparent"
        renderAccessory={() => <TextInput.Icon name="chevron-down" />}
        containerStyle={styles.pickerStyle}
        style={styles.pickerInput}
      />

      <Button mode="contained" onPress={onPress}>
        {i18n.t('next_label')}
      </Button>
    </View>
  );
};

const mapStateToProps = createStructuredSelector({
  data: selectData,
  errorData: selectErrorData,
  loading: selectLoadingStatus,
  postData: selectPostData,
});

const mapDispatchToPrpos = {
  GetServiceTypes: getServiceTypes,
  SetPostData: setPostData,
};

export default connect(mapStateToProps, mapDispatchToPrpos)(CarrierInformation);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: 8,
  },

  pickerStyle: {
    // flex: 1,
    borderRadius: 4,
    borderWidth: 1.5,
    marginBottom: 12,
  },
  pickerInput: {
    marginBottom: 0,
    backgroundColor: '#fff',
  },
});

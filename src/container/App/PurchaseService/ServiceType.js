import React, { useCallback, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { ScrollView, StyleSheet, View } from 'react-native';
import { Dropdown } from 'react-native-material-dropdown-v2-fixed';

import { Caption, Button, Title, Text, RadioButton } from 'react-native-paper';

import { i18n } from '@i18n';
import AppContainer from '@components/AppContainer';

import {
  selectData,
  getServiceTypes,
  selectErrorData,
  selectErrorStatus,
  selectLoadingStatus,
} from '@redux/reducers/purchaseService/getServiceType';

import { selectPostData, setPostData } from '@redux/reducers/auth/registration';

const PersonalForm = props => {
  const { data, GetServiceTypes, loading, onPress, SetPostData, postData } =
    props;

  useEffect(() => {
    GetServiceTypes();
  }, [GetServiceTypes]);

  const renderOptions = (item, index) => {
    return (
      <View style={styles.radioOption} key={index}>
        <RadioButton value={item.id} />
        <View>
          <Text>{item.name}</Text>
          {!!item.short_description && (
            <Caption>{item.short_description}</Caption>
          )}
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <Title>{i18n.t('service_type_label')}</Title>
      <Caption style={{ marginBottom: 16 }}>
        {i18n.t('service_type_caption')}
      </Caption>

      {data && (
        <RadioButton.Group
          onValueChange={newValue => SetPostData('service_type', newValue)}
          value={postData.service_type}>
          {data.request_types.map(renderOptions)}
        </RadioButton.Group>
      )}
      <Button mode="contained" onPress={onPress}>
        {i18n.t('next_label')}
      </Button>
    </View>
  );
};

const mapStateToProps = createStructuredSelector({
  data: selectData,
  errorData: selectErrorData,
  loading: selectLoadingStatus,
  postData: selectPostData,
});

const mapDispatchToPrpos = {
  GetServiceTypes: getServiceTypes,
  SetPostData: setPostData,
};

export default connect(mapStateToProps, mapDispatchToPrpos)(PersonalForm);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: 8,
  },
  row: {
    flexDirection: 'row',
  },
  scroll: {
    flexGrow: 1,
  },
  pickerStyle: { flex: 1, marginRight: 8, paddingVertical: 0 },
  textInput: {
    marginBottom: 12,
  },
  textLabel: {
    fontSize: 16,
  },
  radioOption: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

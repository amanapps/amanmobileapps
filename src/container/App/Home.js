import { connect } from 'react-redux';
import { Avatar, Text, TouchableRipple } from 'react-native-paper';
import Carousel from 'react-native-snap-carousel';
import { createStructuredSelector } from 'reselect';
import { ScrollView } from 'react-native-gesture-handler';
import React, { useCallback, useEffect, useState } from 'react';
import { Dimensions, View, StyleSheet, Image } from 'react-native';

import Loader from '@components/Loader';
import Author from '@components/Author';
import AppContainer from '@components/AppContainer';

const SLIDER_WIDTH = Dimensions.get('window').width;
const ITEM_WIDTH = Math.round(SLIDER_WIDTH * 0.88);
const ITEM_HEIGHT = Math.round((ITEM_WIDTH * 3) / 4);

import {
  getAds,
  selectData as selectAds,
  selectErrorData as selectAdsErrorData,
  selectErrorStatus as selectAdsErrorStatus,
  selectLoadingStatus as selectAdsLoadingStatus,
} from '../../redux/reducers/misc/advertisement';

import {
  getAdsList,
  selectData as selectAdsListData,
  selectErrorData as selectAdsListErrorData,
  selectErrorStatus as selectAdsListErrorStatus,
  selectLoadingStatus as selectAdsListLoadingStatus,
} from '../../redux/reducers/misc/advertisementList';

// import { selectData as selectUserId } from '@store/reducers/auth/setUserID';
// import { showLoader, hideLoader } from '@store/reducers/rootLoaderReducer';

const Home = ({ navigation, ...props }) => {
  const [index, setIndex] = useState(0);
  const {
    // drawerProps,
    // Hidedrawer,
    // GetUserDetails,
    // currentUser,
    // userLoading,
    // ShowModal,
    // HideModal,
    adsData,
    GetAds,
    adsLoading,
    GetAdsList,
    adsListData,
    adsListLoading,
  } = props;

  useEffect(() => {
    GetAds();
    GetAdsList();
  }, [GetAds, GetAdsList]);

  const onAdsClick = useCallback(
    item => {
      navigation.navigate('LargeAds', { item });
    },
    [navigation],
  );

  const _renderItem = useCallback(
    ({ item }) => (
      <TouchableRipple onPress={() => onAdsClick(item)}>
        <Image
          source={{ uri: `${item.image_url}?download=True` }}
          onError={({ nativeEvent: { error } }) => console.log('image', error)}
          style={styles.itemContainer}>
          {/* <Text style={[styles.bold, styles.itemLabel]}>{item.name}</Text> */}
        </Image>
      </TouchableRipple>
    ),
    [onAdsClick],
  );

  // useEffect(() => {
  //   // set toggleDrawer function in params
  //   if (drawerProps.show) {
  //     navigation.toggleDrawer();
  //     setTimeout(() => {
  //       Hidedrawer();
  //     }, 1000);
  //   }
  // }, [Hidedrawer, drawerProps.show, navigation]);

  // useEffect(() => {
  //   if (userLoading) {
  //     ShowModal();
  //   } else {
  //     HideModal();
  //   }
  // }, [HideModal, ShowModal, userLoading]);

  // useEffect(() => GetUserDetails(currentUser), [GetUserDetails, currentUser]);

  if (adsLoading) {
    return <Loader />;
  }

  return (
    <ScrollView>
      <AppContainer style={styles.wrapper}>
        <Author width={ITEM_WIDTH} />

        {/* <View style={styles.carouselContainer}>
          <Carousel
            data={adsData.Response}
            renderItem={_renderItem}
            sliderWidth={SLIDER_WIDTH}
            itemWidth={ITEM_WIDTH}
            inactiveSlideShift={0}
            onSnapToItem={setIndex}
            useScrollView={true}
          />
        </View> */}

        {adsListLoading ? (
          <Loader />
        ) : (
          <View style={styles.carouselContainer}>
            <Carousel
              data={adsListData.Response}
              renderItem={_renderItem}
              sliderWidth={SLIDER_WIDTH}
              itemWidth={ITEM_WIDTH}
              inactiveSlideShift={0}
              onSnapToItem={setIndex}
              useScrollView={true}
              autoplay
              loop
              autoplayInterval={3000}
            />
          </View>
        )}
      </AppContainer>
    </ScrollView>
  );
};

const mapStateToProps = createStructuredSelector({
  // drawerProps: selectBottomSheetProps,
  // currentUser: selectUserId,
  adsData: selectAds,
  adsErrorData: selectAdsErrorData,
  adsLoading: selectAdsLoadingStatus,

  adsListData: selectAdsListData,
  adsListErrorData: selectAdsListErrorData,
  adsListLoading: selectAdsListLoadingStatus,
});

const mapDispatchToProps = {
  GetAds: getAds,
  GetAdsList: getAdsList,
  // Hidedrawer: hidedrawer,
  // GetUserDetails: getUserDetails,
  // ShowModal: showLoader,
  // HideModal: hideLoader,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);

const styles = StyleSheet.create({
  wrapper: {},
  carouselContainer: {
    marginVertical: 12,
  },
  itemContainer: {
    width: ITEM_WIDTH,
    height: ITEM_HEIGHT,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#BEBDBD',
    borderRadius: 10,
    overflow: 'hidden',
  },
  itemLabel: {
    fontSize: 19,
    lineHeight: 27,
  },
});

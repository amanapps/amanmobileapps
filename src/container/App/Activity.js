import { connect } from 'react-redux';
import React, { useState, useEffect, useCallback } from 'react';
import { createStructuredSelector } from 'reselect';
import { View, StyleSheet } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import DropDown from 'react-native-paper-dropdown';
import Geolocation from 'react-native-geolocation-service';

import { ScrollView } from 'react-native-gesture-handler';
import { TextInput, RadioButton, Text, Button } from 'react-native-paper';

import { i18n } from '@i18n';

import Loader from '@components/Loader';
import AppContainer from '@components/AppContainer';

// import { selectData as selectUserId } from '@store/reducers/auth/setUserID';
import { showLoader, hideLoader } from '@redux/reducers/rootLoaderReducer';

import {
  getWorkTypeList,
  selectLoadingStatus,
  selectData,
  getWorkTypeListReset,
} from '../../redux/reducers/misc/getWorkTypes';

import {
  setActivityLog,
  selectLoadingStatus as activityLoading,
  selectData as selectActivityData,
  selectErrorData as selectActivityError,
  reset as activtyReset,
} from '../../redux/reducers/committe/activityLog';

import { dateformat } from '../../utils/global';
import { GeoLocator } from '../../components/GeoLocator';

const Activity = ({ navigation, ...props }) => {
  const {
    // drawerProps,
    // Hidedrawer,
    // GetUserDetails,
    // currentUser,
    GetWorkTypes,
    WorkListReset,
    workTypes,

    activityLoading,
    activtyData,
    activityError,
    SetLog,

    loading,
    ShowModal,
    HideModal,
  } = props;

  const [formData, setFormData] = useState({ committee_id: '1' });

  const [height, setHeight] = useState(56);
  const [height2, setHeight2] = useState(56);

  const [date, setDate] = useState(null);
  const [show, setShow] = useState(false);

  const [showDropDown, setShowDropDown] = useState(false);

  const onSetFormData = useCallback(
    field_name => text => {
      setFormData({
        ...formData,
        [field_name]: text,
      });
    },
    [formData],
  );

  useEffect(() => {
    if (activtyData) {
      setFormData({});
    }
  }, [activtyData, setFormData]);

  const onPressButton = useCallback(() => {
    console.log(formData);
    SetLog({ ...formData, location: formData.locationsss });
  }, [SetLog, formData]);

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(false);
    setDate(currentDate);

    onSetFormData('activity_date')(dateformat(currentDate, 'dd-mm-yyyy'));
  };

  useEffect(() => {
    GetWorkTypes();
    return WorkListReset;
  }, [GetWorkTypes, WorkListReset]);

  useEffect(() => {
    if (loading || activityLoading) {
      ShowModal();
    } else {
      HideModal();
    }
  }, [HideModal, ShowModal, loading, activityLoading]);

  useEffect(() => {
    if (activityError && Object.keys(activityError).length) {
      alert(JSON.stringify(activityError));
    }
  }, [activityError]);

  const mode = 'contained';
  const theme = { roundness: 16, colors: { text: '#000' } };
  const radiotheme = {
    roundness: 16,
    colors: { primary: '#000', text: '#000' },
  };
  return (
    <ScrollView contentContainerStyle={styles.contentContainerStyle}>
      <AppContainer style={styles.wrapper}>
        <View style={{ marginHorizontal: 16 }}>
          <TextInput
            theme={theme}
            mode={mode}
            underlineColor="transparent"
            style={styles.input}
            label={i18n.t('commite')}
            value={formData.committee_id}
            onChangeText={onSetFormData('committee_id')}
          />

          <TextInput
            theme={theme}
            mode={mode}
            underlineColor="transparent"
            style={styles.input}
            label={i18n.t('location')}
            value={formData.locationsss}
            onChangeText={onSetFormData('locationsss')}
          />
          {/* <GeoLocator
            mode={mode}
            theme={theme}
            style={styles.input}
            value={JSON.stringify(formData.location)}
            underlineColor="transparent"
            label={i18n.t('location')}
            onSetLocation={data => {
              console.log('DAta', data);
              onSetFormData('location')(data);
            }}
          /> */}
          <TextInput
            theme={theme}
            mode={mode}
            underlineColor="transparent"
            label={i18n.t('shop_name')}
            value={formData.shop_name}
            onChangeText={onSetFormData('shop_name')}
            style={styles.input}
          />
          <TextInput
            theme={theme}
            mode={mode}
            underlineColor="transparent"
            label={i18n.t('specialization')}
            value={formData.specialisation}
            onChangeText={onSetFormData('specialisation')}
            style={styles.input}
          />

          <DropDown
            label={i18n.t('type_of_work')}
            mode="mode"
            theme={theme}
            inputProps={{ style: styles.input, underlineColor: 'transparent' }}
            visible={showDropDown}
            showDropDown={() => setShowDropDown(true)}
            onDismiss={() => setShowDropDown(false)}
            value={formData.work_type_id}
            setValue={onSetFormData('work_type_id')}
            list={workTypes}
          />

          <TextInput
            theme={theme}
            mode={mode}
            underlineColor="transparent"
            label={i18n.t('visit_report')}
            multiline
            value={formData.visit_report}
            onChangeText={onSetFormData('visit_report')}
            onContentSizeChange={event => {
              setHeight(event.nativeEvent.contentSize.height);
            }}
            style={[styles.input, { height }]}
          />

          <View style={styles.row}>
            <Text theme={radiotheme}>Safety Conditions</Text>
            <View style={{ flex: 1, marginVertical: 16 }}>
              <RadioButton.Group
                value={formData.safety_conditions}
                onValueChange={onSetFormData('safety_conditions')}>
                <View style={styles.radioContainer}>
                  <View style={styles.radioOption}>
                    <RadioButton value="True" theme={radiotheme} />
                    <Text theme={radiotheme}>{i18n.t('yes_label')}</Text>
                  </View>
                  <View style={styles.radioOption}>
                    <RadioButton value="False" theme={radiotheme} />
                    <Text theme={radiotheme}>{i18n.t('no_label')}</Text>
                  </View>
                </View>
              </RadioButton.Group>
            </View>
          </View>

          <View style={styles.row}>
            <Text theme={radiotheme}>Social Security Record</Text>
            <View style={{ flex: 1, marginVertical: 0 }}>
              <RadioButton.Group
                value={formData.social_security_record}
                onValueChange={onSetFormData('social_security_record')}>
                <View style={styles.radioContainer}>
                  <View style={styles.radioOption}>
                    <RadioButton value="True" theme={radiotheme} />
                    <Text theme={radiotheme}>{i18n.t('yes_label')}</Text>
                  </View>
                  <View style={styles.radioOption}>
                    <RadioButton value="False" theme={radiotheme} />
                    <Text theme={radiotheme}>{i18n.t('no_label')}</Text>
                  </View>
                </View>
              </RadioButton.Group>
            </View>
          </View>
          <TextInput
            mode={mode}
            theme={theme}
            underlineColor="transparent"
            label={i18n.t('name_of_the_employee')}
            value={formData.employee_names}
            onChangeText={onSetFormData('employee_names')}
            onContentSizeChange={event => {
              setHeight2(event.nativeEvent.contentSize.height);
            }}
            style={[styles.input, { height: height2 }]}
          />
          <Text
            theme={theme}
            onPress={() => setShow(true)}
            style={styles.inputTypeText}>
            {date ? dateformat(date, 'dd-MMM-yyyy') : i18n.t('activity_date')}
          </Text>
          <Button
            mode="contained"
            style={styles.buttonStyle}
            labelStyle={styles.buttonLabel}
            onPress={onPressButton}
            theme={{ colors: { primary: '#40DCCD' } }}>
            Submit
          </Button>
          {show && (
            <DateTimePicker
              mode={mode}
              display="default"
              onChange={onChange}
              testID="dateTimePicker"
              value={date || new Date()}
            />
          )}
        </View>
      </AppContainer>
    </ScrollView>
  );
};

const mapStateToProps = createStructuredSelector({
  workTypes: selectData,
  loading: selectLoadingStatus,

  activityLoading,
  activtyData: selectActivityData,
  activityError: selectActivityError,
});

const mapDispatchToProps = {
  // Hidedrawer: hidedrawer,
  // GetUserDetails: getUserDetails,
  GetWorkTypes: getWorkTypeList,
  WorkListReset: getWorkTypeListReset,

  SetLog: setActivityLog,
  ResetLog: activtyReset,

  ShowModal: showLoader,
  HideModal: hideLoader,
};

export default connect(mapStateToProps, mapDispatchToProps)(Activity);

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: '#fff',
  },
  contentContainerStyle: {
    flexGrow: 1,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  margin: {
    marginVertical: 10,
  },
  input: {
    marginBottom: 8,
    borderRadius: 16,
    fontFamily: 'Poppins-Medium',
  },
  inputTypeText: {
    backgroundColor: '#e9e9e9',
    marginBottom: 8,
    borderRadius: 16,
    paddingHorizontal: 16,
    paddingVertical: 16,
    lineHeight: 24,
    fontFamily: 'Poppins-Regular',
  },
  last: { marginBottom: 50 },
  radioContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  radioOption: { flexDirection: 'row', alignItems: 'center' },
  buttonLabel: {
    color: '#000',
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
  },
  buttonStyle: {
    elevation: 0,
    color: '#000',
    borderRadius: 36,
    alignSelf: 'flex-start',
    paddingHorizontal: 36,
    marginVertical: 16,
  },
});

import { connect } from 'react-redux';
import { TextInput, Button } from 'react-native-paper';
import React, { useCallback, useEffect, useState } from 'react';
import { Dimensions, View, StyleSheet, ImageBackground } from 'react-native';

import { i18n } from '@i18n';
import { apiDispatcher } from '../../api';

import { showModal } from '@redux/reducers/modals';

import AppContainer from '@components/AppContainer';

const Contact = props => {
  const { Show } = props;

  const [message, setMessage] = useState('');
  const [loading, setLoading] = useState(false);

  const onPressButton = useCallback(() => {
    setLoading(true);
    apiDispatcher('contact_us', { message }).then(response => {
      setLoading(false);
      const { status, data } = response;
      console.log(data);
      if (status > 0 && status < 400) {
        // set current data
        setMessage(null);
        Show('INFORMATION_MODAL', { message: data.msg, mode: 'success' });
      } else {
        Show('INFORMATION_MODAL', { message: data.msg, mode: 'error' });
      }
    });
  }, [setLoading, Show, message]);

  const roundness = 16;
  const mode = 'outlined';
  return (
    <AppContainer style={{ paddingHorizontal: 16 }}>
      <TextInput
        theme={{ roundness, colors: { text: '#000' } }}
        mode={mode}
        label={i18n.t('message')}
        value={message}
        multiline
        onChangeText={setMessage}
        style={[styles.input]}
      />

      <Button
        mode="contained"
        style={styles.buttonStyle}
        labelStyle={styles.buttonLabel}
        onPress={onPressButton}
        loading={loading}
        disabled={loading || !message}
        theme={{ colors: { primary: '#40DCCD' } }}>
        {i18n.t('submit_label')}
      </Button>
    </AppContainer>
  );
};

const styles = StyleSheet.create({
  input: {
    marginBottom: 8,
    borderRadius: 16,
    fontFamily: 'Poppins-Medium',
  },
  buttonLabel: {
    color: '#000',
    fontFamily: 'Poppins-Regular',
    fontSize: 15,
  },
  buttonStyle: {
    elevation: 0,
    color: '#000',
    alignSelf: 'center',
    borderRadius: 36,
    paddingHorizontal: 36,
    marginVertical: 16,
  },
});

const mapDispatchToPrpos = {
  Show: showModal,
};

export default connect(null, mapDispatchToPrpos)(Contact);

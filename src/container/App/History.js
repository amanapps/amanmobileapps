import { connect } from 'react-redux';
import { Appbar, Avatar, Card, Colors, Text } from 'react-native-paper';
import { createStructuredSelector } from 'reselect';
import { FlatList, ScrollView } from 'react-native-gesture-handler';
import React, { useCallback, useEffect, useState } from 'react';
import {
  Dimensions,
  View,
  StyleSheet,
  ImageBackground,
  RefreshControl,
} from 'react-native';

import { i18n } from '@i18n';

import Loader from '@components/Loader';
import Author from '@components/Author';
import AppContainer from '@components/AppContainer';

import {
  getTransactionInvoices,
  selectData,
  selectErrorData,
  selectErrorStatus,
  selectLoadingStatus,
} from '../../redux/reducers/transaction/history';

// import { selectData as selectUserId } from '@store/reducers/auth/setUserID';
import { showLoader, hideLoader } from '@redux/reducers/rootLoaderReducer';

const SearchResult = props => {
  const { amount_total, invoice_date, name, invoice_payment_state } = props;
  return (
    <Card
      theme={{ roundness: 30 }}
      elevation={12}
      style={{ marginVertical: 16 }}>
      <Card.Content style={{ margin: 4 }}>
        <View style={styles.row}>
          <View style={{ flex: 1 }}>
            <Text style={styles.bigTitle}>{name}</Text>
          </View>
          <View>
            <Text>
              {i18n.t('invoice_status', {
                status: invoice_payment_state || '',
              })}
            </Text>
          </View>
        </View>
        <View style={[styles.row, styles.margin]}>
          <View style={{ flex: 1 }}>
            <Text style={styles.value}>
              {i18n.t('invoice_date', { invoice_date: invoice_date || '' })}
            </Text>
          </View>
          <View style={{ flex: 1, alignItems: 'flex-end' }}>
            <Text style={styles.value}>
              {i18n.t('amount_total', { amount_total: amount_total || '' })}
            </Text>
          </View>
        </View>
      </Card.Content>
    </Card>
  );
};
const History = ({ navigation, ...props }) => {
  const { getTransactionInvoices, ShowModal, HideModal, data, loading } = props;

  const _renderItem = useCallback(({ item }) => <SearchResult {...item} />, []);

  // useEffect(() => {
  //   // set toggleDrawer function in params
  //   if (drawerProps.show) {
  //     navigation.toggleDrawer();
  //     setTimeout(() => {
  //       Hidedrawer();
  //     }, 1000);
  //   }
  // }, [Hidedrawer, drawerProps.show, navigation]);

  useEffect(() => onRefresh(), [onRefresh]);

  const onRefresh = useCallback(
    () => getTransactionInvoices(),
    [getTransactionInvoices],
  );

  useEffect(() => {
    if (loading) {
      ShowModal();
    } else {
      HideModal();
    }
  }, [HideModal, ShowModal, loading]);

  const showList = !loading && data.length > 0;
  return (
    <ScrollView contentContainerStyle={styles.contentContainerStyle}>
      <AppContainer style={styles.wrapper}>
        <View style={{ marginHorizontal: 16 }}>
          <Author width="100%" />
        </View>

        {showList && (
          <FlatList
            data={data}
            renderItem={_renderItem}
            refreshControl={
              <RefreshControl refreshing={loading} onRefresh={onRefresh} />
            }
            contentContainerStyle={styles.flatListContainer}
            keyExtractor={item => item.id}
          />
        )}

        {!showList && (
          <Text
            style={[
              styles.title,
              {
                color: Colors.red400,
                alignSelf: 'center',
                paddingVertical: 16,
              },
            ]}>
            {i18n.t('no_data_found')}
          </Text>
        )}
      </AppContainer>
    </ScrollView>
  );
};

const mapStateToProps = createStructuredSelector({
  data: selectData,
  errorData: selectErrorData,
  loading: selectLoadingStatus,
});

const mapDispatchToProps = {
  getTransactionInvoices,
  ShowModal: showLoader,
  HideModal: hideLoader,
};

export default connect(mapStateToProps, mapDispatchToProps)(History);

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: '#fff',
  },
  contentContainerStyle: {
    flexGrow: 1,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
  },
  margin: {
    marginVertical: 10,
  },
  searchBar: {
    borderRadius: 28,
    backgroundColor: '#F2F2F2',
    elevation: 0,
    marginVertical: 16,
    marginHorizontal: 16,
  },
  bigTitle: {
    color: '#4A4B4D',
    fontSize: 22,
    lineHeight: 30,
    fontFamily: 'Poppins-Bold',
  },
  title: {
    color: '#4A4B4D',
    fontSize: 14,
    lineHeight: 21,
    fontFamily: 'Poppins-Bold',
  },
  value: {
    color: '#4A4B4D',
    fontSize: 14,
    lineHeight: 21,
    fontFamily: 'Poppins-Light',
  },
  flagContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  flatListContainer: {
    paddingHorizontal: 16,
  },
});

import React, { useCallback, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { ScrollView, StyleSheet, View } from 'react-native';

import Logo from '@components/Logo';
import ESCOLogo from '@components/ESCOLogo';
import AppContainer from '@components/AppContainer';

import {
  merchantRegisterUser as registerUser,
  selectData,
  selectErrorData,
  selectErrorStatus,
  selectLoadingStatus,
  selectPostData,
  reset,
} from '@redux/reducers/auth/registration';
import { showModal } from '@redux/reducers/modals';
import { setLoginPath } from '@redux/reducers/misc/setLoginPath';

import PersonalForm from './PersonalForm';

const Registration = props => {
  const {
    Register,
    postData,
    loading,
    errorData,
    Show,
    data,
    navigation,
    SetLoginPath,
    Reset,
  } = props;

  useEffect(() => {
    if (errorData) {
      Show('INFORMATION_MODAL', { mode: 'error', message: errorData.msg });
    }
  }, [errorData, Show]);

  useEffect(() => {
    if (data) {
      navigation.navigate('OTP', {
        mobile: postData.mobile,
        postData: postData,
      });
      // SetLoginPath('App');
    }
  }, [SetLoginPath, postData, navigation, data]);

  useEffect(() => {
    return Reset;
  }, [Reset]);

  return (
    <ScrollView contentContainerStyle={styles.scroll}>
      <AppContainer style={styles.container}>
        {/* <Logo style={styles.logo} /> */}
        <PersonalForm onPress={Register} disabled={loading} />
        {/* <View style={styles.escoContainer}>
          <ESCOLogo style={styles.escoLogo} />
        </View> */}
      </AppContainer>
    </ScrollView>
  );
};

const mapStateToProps = createStructuredSelector({
  data: selectData,
  errorData: selectErrorData,
  loading: selectLoadingStatus,
  postData: selectPostData,
  // sentData: sendOTPSelectData,
  // sentError: sendOTPSelectErrorData,
  // sentLoading: sendOTPSelectLoadingStatus,
});

const mapDispatchToPrpos = {
  Register: registerUser,
  Show: showModal,
  SetLoginPath: setLoginPath,
  Reset: reset,
};

export default connect(mapStateToProps, mapDispatchToPrpos)(Registration);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: 16,
  },
  row: {
    flexDirection: 'row',
  },
  scroll: {
    flexGrow: 1,
  },
  logo: {
    width: 200,
    alignSelf: 'center',
  },
  textInput: {
    marginBottom: 12,
  },
  textLabel: {
    fontSize: 16,
  },
  radioOption: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  escoContainer: {
    flex: 1,
    marginVertical: 16,
    justifyContent: 'center',
    alignItems: 'center',
  },
  escoLogo: {},
});

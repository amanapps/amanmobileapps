import React, { useCallback, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { ScrollView, StyleSheet, View } from 'react-native';
import DropDown from 'react-native-paper-dropdown';

import {
  Caption,
  Button,
  Title,
  TextInput,
  Text,
  RadioButton,
  Menu,
} from 'react-native-paper';

import { i18n } from '@i18n';
import Loader from '@components/Loader';
import ESCOLogo from '@components/ESCOLogo';
import TextField from '@components/TextField';
import AppContainer from '@components/AppContainer';

import { selectPostData, setPostData } from '@redux/reducers/auth/registration';

import {
  selectData,
  getCountryStates,
  selectErrorData,
  selectErrorStatus,
  selectLoadingStatus,
} from '@redux/reducers/misc/getCountryStates';

import {
  selectData as businessTypeData,
  getBusinessType,
  selectErrorData as businessTypeErrorData,
  selectErrorStatus as businessTypeErrorStatus,
  selectLoadingStatus as businessTypeLoading,
} from '../../../redux/reducers/misc/getBussinessType';

import {
  selectData as unionTypeData,
  getUnion,
  selectErrorData as unionTypeErrorData,
  selectErrorStatus as unionTypeErrorStatus,
  selectLoadingStatus as unionTypeLoading,
} from '../../../redux/reducers/misc/getUnion';

const PersonalForm = props => {
  const {
    postData,
    SetPostData,
    onPress,
    GetCountryStates,
    data: sData,
    loading,
    disabled,

    businessTypeData: bData,
    businessTypeLoading: bLoading,
    GetBusinessType,

    unionTypeData: uData,
    unionTypeLoading: uLoading,
    GetUnion,
  } = props;

  const [showStateDropDown, setShowStateDropDown] = useState(false);
  const [showBusinessDropDown, setShowBusinessDropDown] = useState(false);
  // const [showUnionDropDown, setShowUnionDropDown] = useState(false);

  useEffect(() => {
    GetCountryStates('106');
    GetBusinessType();
    // GetUnion();
  }, [GetCountryStates, GetBusinessType]);

  if (loading || bLoading || uLoading) {
    return <Loader />;
  }

  const theme = { roundness: 16, colors: { text: '#000' } };

  return (
    <>
      <Text style={styles.title}>{i18n.t('registration_heading')}</Text>
      <Caption style={styles.caption}>{i18n.t('registration_caption')}</Caption>

      <View style={{ height: 16 }} />
      <TextInput
        mode="contained"
        underlineColor="transparent"
        theme={theme}
        style={styles.textInput}
        value={postData.name}
        onChangeText={text => SetPostData('name', text)}
        label={i18n.t('registration_full_name_label')}
        placeholder={i18n.t('registration_full_name_placholder')}
      />

      <TextInput
        mode="contained"
        underlineColor="transparent"
        theme={theme}
        style={styles.textInput}
        value={postData.mobile}
        maxLength={10}
        keyboardType="number-pad"
        onChangeText={text => SetPostData('mobile', text)}
        label={i18n.t('registration_mobile_number_label')}
        placeholder={i18n.t('registration_mobile_number_placeholder')}
      />

      <DropDown
        label={i18n.t('registration_business_type_label')}
        mode="mode"
        theme={theme}
        inputProps={{ style: styles.textInput, underlineColor: 'transparent' }}
        visible={showBusinessDropDown}
        showDropDown={() => setShowBusinessDropDown(true)}
        onDismiss={() => setShowBusinessDropDown(false)}
        value={postData.business_type_id}
        setValue={v1 => SetPostData('business_type_id', v1)}
        list={bData}
      />

      {/* <DropDown
        label="Related Union"
        mode="mode"
        theme={theme}
        inputProps={{ style: styles.textInput, underlineColor: 'transparent' }}
        visible={showUnionDropDown}
        showDropDown={() => setShowUnionDropDown(true)}
        onDismiss={() => setShowUnionDropDown(false)}
        value={postData.union_id}
        setValue={v1 => SetPostData('union_id', v1)}
        list={uData}
      /> */}

      <TextInput
        mode="contained"
        underlineColor="transparent"
        theme={theme}
        style={styles.textInput}
        value={postData.password}
        secureTextEntry
        onChangeText={text => SetPostData('password', text)}
        label={i18n.t('registration_password_label')}
        placeholder={i18n.t('registration_password_placeholder')}
      />

      <TextInput
        mode="contained"
        underlineColor="transparent"
        theme={theme}
        style={styles.textInput}
        value={postData.street}
        onChangeText={text => SetPostData('street', text)}
        label={i18n.t('registration_street_one_label')}
        placeholder={i18n.t('registration_street_one_label')}
      />

      <TextInput
        mode="contained"
        underlineColor="transparent"
        theme={theme}
        style={styles.textInput}
        value={postData.street2}
        onChangeText={text => SetPostData('street2', text)}
        label={i18n.t('registration_street_two_label')}
        placeholder={i18n.t('registration_street_two_label')}
      />

      <TextInput
        mode="contained"
        underlineColor="transparent"
        theme={theme}
        style={styles.textInput}
        value={postData.location}
        onChangeText={text => SetPostData('location', text)}
        label={i18n.t('registration_defined_quater_label')}
        placeholder={i18n.t('registration_defined_quater_label')}
      />

      <DropDown
        label="State/Province"
        mode="mode"
        theme={theme}
        inputProps={{ style: styles.textInput, underlineColor: 'transparent' }}
        visible={showStateDropDown}
        showDropDown={() => setShowStateDropDown(true)}
        onDismiss={() => setShowStateDropDown(false)}
        value={postData.state_code}
        setValue={v1 => SetPostData('state_code', v1)}
        list={sData}
      />

      <Button
        mode="contained"
        onPress={onPress}
        disabled={disabled}
        loading={disabled}
        theme={{
          roundness: 36,
          colors: { primary: '#40DCCD' },
          fonts: { medium: 'Poppins-Bold' },
        }}
        style={{ marginBottom: 80 }}>
        {i18n.t('sign_up')}
      </Button>
    </>
  );
};

const mapStateToProps = createStructuredSelector({
  postData: selectPostData,
  data: selectData,
  errorData: selectErrorData,
  loading: selectLoadingStatus,

  businessTypeLoading,
  businessTypeErrorData,
  businessTypeData,

  unionTypeLoading,
  unionTypeErrorData,
  unionTypeData,
});

const mapDispatchToPrpos = {
  SetPostData: setPostData,
  GetCountryStates: getCountryStates,
  GetBusinessType: getBusinessType,
  GetUnion: getUnion,
};

export default connect(mapStateToProps, mapDispatchToPrpos)(PersonalForm);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: 16,
  },
  row: {
    flexDirection: 'row',
  },
  scroll: {
    flexGrow: 1,
  },
  pickerStyle: { flex: 1, marginRight: 8, paddingVertical: 0 },
  textInput: {
    marginBottom: 8,
    borderRadius: 16,
    fontFamily: 'Poppins-Medium',
  },
  textLabel: {
    fontSize: 16,
  },
  radioOption: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  escoContainer: {
    flex: 1,
    marginVertical: 16,
    justifyContent: 'center',
    alignItems: 'center',
  },
  escoLogo: {},
  title: {
    fontFamily: 'Poppins-Bold',
    fontSize: 30,
    color: '#4A4B4D',
    lineHeight: 46,
    alignSelf: 'center',
  },
  caption: {
    fontFamily: 'Poppins-Medium',
    alignSelf: 'center',
    fontSize: 14,
    color: '#4A4B4D',
    lineHeight: 21,
  },
});

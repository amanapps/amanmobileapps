import React, { useCallback, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { ScrollView, StyleSheet, View } from 'react-native';
import { Dropdown } from 'react-native-material-dropdown-v2-fixed';

import {
  Caption,
  Button,
  Title,
  TextInput,
  Text,
  RadioButton,
  Menu,
} from 'react-native-paper';

import { i18n } from '@i18n';
import Loader from '@components/Loader';
import ESCOLogo from '@components/ESCOLogo';
import TextField from '@components/TextField';
import AppContainer from '@components/AppContainer';

import { selectPostData, setPostData } from '@redux/reducers/auth/registration';

import {
  selectData,
  getCountryStates,
  selectErrorData,
  selectErrorStatus,
  selectLoadingStatus,
} from '@redux/reducers/misc/getCountryStates';

let date = [...Array(31).keys()].map(value => ({ value: value + 1 }));
let month = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec',
].map(value => ({ value }));

let d = new Date();
let year = [...Array(100).keys()].map(value => ({
  value: d.getFullYear() - (100 - value),
}));

let country = [{ key: 106, value: 'Iraq' }];

const PersonalForm = props => {
  const {
    postData,
    SetPostData,
    onPress,
    GetCountryStates,
    data,
    loading,
    disabled,
  } = props;

  useEffect(() => {
    GetCountryStates('106');
  }, [GetCountryStates]);

  if (loading) {
    return <Loader />;
  }

  const theme = { roundness: 16, colors: { text: '#000' } };

  return (
    <>
      <Text style={styles.title}>{i18n.t('registration_heading')}</Text>
      <Caption style={styles.caption}>{i18n.t('registration_caption')}</Caption>

      <View style={{ height: 16 }} />
      <TextInput
        mode="outlined"
        theme={theme}
        style={styles.textInput}
        value={postData.name}
        onChangeText={text => SetPostData('name', text)}
        label={i18n.t('registration_full_name_label')}
        placeholder={i18n.t('registration_full_name_placholder')}
      />

      <TextInput
        mode="outlined"
        theme={theme}
        style={styles.textInput}
        value={postData.mobile}
        maxLength={11}
        keyboardType="number-pad"
        onChangeText={text => SetPostData('mobile', text)}
        label={i18n.t('registration_mobile_number_label')}
        placeholder={i18n.t('registration_mobile_number_placeholder')}
      />

      <TextInput
        mode="outlined"
        theme={theme}
        style={styles.textInput}
        value={postData.password}
        secureTextEntry
        onChangeText={text => SetPostData('password', text)}
        label={i18n.t('registration_password_label')}
        placeholder={i18n.t('registration_password_placeholder')}
      />

      <Title style={styles.textLabel}>{i18n.t('registration_dob_label')}</Title>
      <View style={styles.row}>
        <Dropdown
          data={date}
          label="Date"
          useNativeDriver
          itemCount={10}
          value={postData.day}
          onChangeText={value => SetPostData('day', value)}
          renderAccessory={() => <TextInput.Icon name="calendar" size={18} />}
          containerStyle={styles.pickerStyle}
        />
        <Dropdown
          data={month}
          label="Month"
          itemCount={10}
          value={postData.month}
          onChangeText={(value, index) => SetPostData('month', index + 1)}
          renderAccessory={() => <TextInput.Icon name="calendar" size={18} />}
          containerStyle={styles.pickerStyle}
        />
        <Dropdown
          data={year}
          label="Year"
          itemCount={10}
          value={postData.year}
          onChangeText={value => SetPostData('year', value)}
          renderAccessory={() => <TextInput.Icon name="calendar" size={18} />}
          containerStyle={styles.pickerStyle}
        />
      </View>

      <Title style={styles.textLabel}>
        {i18n.t('registration_gender_label')}
      </Title>
      <RadioButton.Group
        onValueChange={value => SetPostData('gender', value)}
        value={postData.gender}>
        <View style={styles.row}>
          <View style={styles.radioOption}>
            <RadioButton value="male" />
            <Text>{i18n.t('registration_gender_option_male_label')}</Text>
          </View>
          <View style={styles.radioOption}>
            <RadioButton value="female" />
            <Text>{i18n.t('registration_gender_option_female_label')}</Text>
          </View>
        </View>
      </RadioButton.Group>

      {/* <Divider /> */}

      <Title style={[styles.textLabel, { marginTop: 16 }]}>
        {i18n.t('registration_full_address_label')}
      </Title>

      <TextInput
        mode="outlined"
        theme={theme}
        style={styles.textInput}
        value={postData.street}
        onChangeText={text => SetPostData('street', text)}
        label={i18n.t('registration_street_one_label')}
        placeholder={i18n.t('registration_street_one_label')}
      />

      <TextInput
        mode="outlined"
        theme={theme}
        style={styles.textInput}
        value={postData.street2}
        onChangeText={text => SetPostData('street2', text)}
        label={i18n.t('registration_street_two_label')}
        placeholder={i18n.t('registration_street_two_label')}
      />

      <TextInput
        mode="outlined"
        theme={theme}
        style={styles.textInput}
        label={i18n.t('registration_defined_quater_label')}
        placeholder={i18n.t('registration_defined_quater_label')}
      />

      {data && (
        <Dropdown
          data={data.states}
          label="State/Province"
          itemCount={10}
          onChangeText={(value, index, arr) => {
            SetPostData('state_id', arr[index].id);
          }}
          renderAccessory={() => <TextInput.Icon name="chevron-down" />}
          containerStyle={styles.pickerStyle}
        />
      )}
      <Dropdown
        data={country}
        label="Conutry"
        itemCount={10}
        value="Iraq"
        renderAccessory={() => <TextInput.Icon name="chevron-down" />}
        containerStyle={styles.pickerStyle}
      />

      <TextInput
        mode="outlined"
        theme={theme}
        style={styles.textInput}
        onChangeText={text => SetPostData('mother_name', text)}
        label={i18n.t('registration_mobile_number_label')}
        placeholder={i18n.t('registration_mobile_number_placeholder')}
      />

      <Button
        mode="contained"
        onPress={onPress}
        disabled={disabled}
        loading={disabled}
        theme={{
          roundness: 36,
          colors: { primary: '#338e86' },
          fonts: { medium: 'Poppins-Bold' },
        }}>
        {i18n.t('sign_up')}
      </Button>
    </>
  );
};

const mapStateToProps = createStructuredSelector({
  postData: selectPostData,
  data: selectData,
  errorData: selectErrorData,
  loading: selectLoadingStatus,
});

const mapDispatchToPrpos = {
  SetPostData: setPostData,
  GetCountryStates: getCountryStates,
};

export default connect(mapStateToProps, mapDispatchToPrpos)(PersonalForm);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: 16,
  },
  row: {
    flexDirection: 'row',
  },
  scroll: {
    flexGrow: 1,
  },
  pickerStyle: { flex: 1, marginRight: 8, paddingVertical: 0 },
  textInput: {
    marginBottom: 8,
    borderRadius: 16,
    fontFamily: 'Poppins-Medium',
  },
  textLabel: {
    fontSize: 16,
  },
  radioOption: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  escoContainer: {
    flex: 1,
    marginVertical: 16,
    justifyContent: 'center',
    alignItems: 'center',
  },
  escoLogo: {},
  title: {
    fontFamily: 'Poppins-Bold',
    fontSize: 30,
    color: '#4A4B4D',
    lineHeight: 46,
  },
  caption: {
    fontFamily: 'Poppins-Medium',
    fontSize: 14,
    color: '#4A4B4D',
    lineHeight: 21,
  },,
});

import React, { useCallback, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { ScrollView, StyleSheet, View } from 'react-native';
import { Button, Divider, Text, TextInput, Title } from 'react-native-paper';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { i18n } from '@i18n';
import constants from '@config/constant';

import Logo from '@components/Logo';
import ESCOLogo from '@components/ESCOLogo';
import TextField from '@components/TextField';
import AppContainer from '@components/AppContainer';
import { updateTokenInHeader } from '@api/AuthApi';

import {
  login,
  selectData,
  loginReset,
  selectErrorData,
  selectErrorStatus,
  selectLoadingStatus,
} from '@redux/reducers/auth/login';

import { showModal } from '@redux/reducers/modals';
import { setLoginPath } from '@redux/reducers/misc/setLoginPath';
import { showLoader, hideLoader } from '@redux/reducers/rootLoaderReducer';
import {
  getUserDetails,
  selectLoadingStatus as userLoading,
} from '../../redux/reducers/user/getDetails';
import { setUserID } from '../../redux/reducers/auth/setUserID';
import { colors, regularFont } from '../../config/theme';

const Login = props => {
  const {
    loading,
    data,
    startLogin,
    navigation,
    ShowModal,
    HideModal,
    SetLoginPath,
    LoginReset,
    errorData,
    Show,
    getProfile,
    setUser,
    userLoading,
  } = props;
  const [mobile, setMobile] = useState('07714848336');
  const [password, setPassword] = useState('b');
  // const [mobile, setMobile] = useState('');
  // const [password, setPassword] = useState('');

  const onPress = useCallback(() => {
    startLogin(mobile, password);
  }, [password, startLogin, mobile]);

  useEffect(() => {
    if (data) {
      console.log('Login repsonse', data);
      // updateTokenInHeader(data.oauth_access_token);
      // updateTokenInHeader('_e38bb1702c1384dec465444fabaeb044a7adf641');
      AsyncStorage.setItem(constants.AUTH_TOKEN, data.oauth_access_token);

      SetLoginPath('App');
      setUser(data.uid);
      getProfile();
      // navigation.navigate('OTP', { mobile });
    }
    return LoginReset;
  }, [SetLoginPath, data, LoginReset, navigation, mobile, getProfile, setUser]);

  // useEffect(() => {
  //   if (loading) {
  //     ShowModal();
  //   } else {
  //     HideModal();
  //   }
  // }, [loading, ShowModal, HideModal]);

  useEffect(() => {
    if (errorData) {
      Show('INFORMATION_MODAL', { ...errorData, mode: 'error' });
    }
  }, [errorData, Show]);

  const navgiateForgotPassword = useCallback(() => {
    navigation.navigate('ForgotPassword');
  }, [navigation]);

  const navgiateRegistration = useCallback(() => {
    navigation.navigate('Registration');
  }, [navigation]);

  return (
    <ScrollView contentContainerStyle={styles.scroll}>
      <AppContainer style={styles.container}>
        <Logo style={styles.logo} />
        <Text style={styles.headline}>{i18n.t('login_heading')}</Text>

        <TextInput
          value={mobile}
          style={styles.input}
          theme={{
            roundness: 36,
            colors: { primary: 'transparent' },
          }}
          underlineColor="transparent"
          left={<TextInput.Icon name="cellphone-android" />}
          onChangeText={setMobile}
          keyboardType="phone-pad"
          placeholder={i18n.t('mobile_field_label')}
        />

        <TextInput
          secureTextEntry
          value={password}
          theme={{
            roundness: 36,
            colors: { primary: 'transparent' },
          }}
          underlineColor="transparent"
          style={styles.input}
          left={<TextInput.Icon name="lock" />}
          onChangeText={setPassword}
          placeholder={i18n.t('password_field_label')}
        />

        <Button
          mode="contained"
          onPress={onPress}
          theme={{
            roundness: 36,
            colors: { primary: '#338e86' },
            fonts: { medium: 'Poppins-Bold' },
          }}
          loading={loading || userLoading}
          labelStyle={styles.buttonLabel}
          disabled={loading || userLoading}>
          {i18n.t('login_button_label')}
        </Button>

        <Divider />

        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
          <Button
            style={styles.forgotButton}
            labelStyle={styles.labelStyle}
            theme={{ colors: { primary: '#00000099' }, ...regularFont }}>
            {i18n.t('dont_have_account')}
          </Button>
          <Button
            theme={{ colors: { primary: '#082FA4' }, ...regularFont }}
            onPress={navgiateRegistration}
            labelStyle={styles.labelStyle}
            style={styles.forgotButton}>
            {i18n.t('registration_label')}
          </Button>
        </View>

        <Button
          theme={{ colors: { primary: '#082FA4' }, ...regularFont }}
          onPress={navgiateForgotPassword}
          labelStyle={styles.labelStyle}
          style={styles.forgotButton}>
          {i18n.t('forgot_password_label')}
        </Button>

        {/* <View style={styles.escoContainer}>
          <ESCOLogo style={styles.escoLogo} />
        </View> */}
      </AppContainer>
    </ScrollView>
  );
};

const mapStateToProps = createStructuredSelector({
  data: selectData,
  errorData: selectErrorData,
  loading: selectLoadingStatus,
  userLoading,
});

const mapDispatchToPrpos = {
  startLogin: login,
  ShowModal: showLoader,
  HideModal: hideLoader,
  Show: showModal,
  SetLoginPath: setLoginPath,
  LoginReset: loginReset,
  getProfile: getUserDetails,
  setUser: setUserID,
};

export default connect(mapStateToProps, mapDispatchToPrpos)(Login);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: 16,
  },
  scroll: {
    flexGrow: 1,
  },
  logo: {
    width: 200,
    alignSelf: 'center',
    marginTop: '11%',
  },
  headline: {
    marginTop: 50,
    marginBottom: 16,
    fontSize: 25,
    color: colors.colorPrimary,
    alignSelf: 'center',
  },
  forgotButton: {
    alignItems: 'center',
    fontSize: 15,
    lineHeight: 23,
    letterSpacing: 0.1,
    marginVertical: 8,
    textTransform: 'capitalize',
  },
  escoContainer: {
    flex: 1,
    marginVertical: 16,
    justifyContent: 'center',
    alignItems: 'center',
  },
  labelStyle: {
    fontSize: 15,
    lineHeight: 23,
    letterSpacing: 0.1,
    textTransform: 'capitalize',
  },
  input: {
    fontFamily: 'Poppins-Regular',
    marginBottom: 16,
    borderBottomEndRadius: 36,
    borderBottomStartRadius: 36,
  },
  buttonLabel: {
    color: '#171717',
    fontSize: 15,
    lineHeight: 23,
    fontFamily: 'Poppins-Bold',
    textTransform: 'capitalize',
  },
});

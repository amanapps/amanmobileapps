import React, { useCallback, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { ScrollView, StyleSheet, View } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Caption, Button, Title, TouchableRipple } from 'react-native-paper';

import { i18n } from '@i18n';
import Logo from '@components/Logo';
import ESCOLogo from '@components/ESCOLogo';
import TextField from '@components/TextField';
import AppContainer from '@components/AppContainer';

import constants from '@config/constant';
import { updateTokenInHeader } from '@api/AuthApi';

import {
  verifyOTP,
  selectData,
  otpReset,
  selectErrorData,
  selectErrorStatus,
  selectLoadingStatus,
} from '@redux/reducers/auth/verifyOTP';

import {
  sendOTP,
  selectData as sendOTPSelectData,
  sendOTPReset,
  selectErrorData as sendOTPSelectErrorData,
  selectLoadingStatus as sendOTPSelectLoadingStatus,
} from '@redux/reducers/auth/sendOTP';

import { showModal } from '@redux/reducers/modals';
import { setLoginPath } from '@redux/reducers/misc/setLoginPath';
import { showLoader, hideLoader } from '@redux/reducers/rootLoaderReducer';

import { getUserDetailsSuccess } from '@redux/reducers/user/getDetails';
import { setUserID } from '@redux/reducers/auth/setUserID';

import { getUserDetails } from '@redux/reducers/user/getDetails';

import {
  login,
  selectData as loginData,
  loginReset,
  selectErrorData as loginError,
  selectLoadingStatus as loginLoading,
} from '@redux/reducers/auth/login';

const OTP = props => {
  const {
    verify,
    loading,
    Show,
    reset,
    data,
    errorData,
    SetLoginPath,
    SendOTP,
    sentData,
    sentError,
    sentLoading,

    ShowModal,
    HideModal,
    route: {
      params: { mobile, postData },
    },

    setProfileSuccess,
    setUser,

    login,
    loginReset,
    loginData,
    loginError,
    loginLoading,

    getProfile,
  } = props;

  const [otp, setOTP] = useState(null);

  useEffect(() => {
    if (sentLoading || loginLoading) {
      ShowModal();
    } else {
      HideModal();
    }
  }, [sentLoading, ShowModal, HideModal, loginLoading]);

  useEffect(() => {
    SendOTP(mobile);
  }, [mobile, SendOTP]);

  useEffect(() => {
    if (data) {
      login(mobile, postData.password);
    }
    return reset;
  }, [
    SetLoginPath,
    data,
    login,
    mobile,
    postData.password,
    reset,
    setProfileSuccess,
    setUser,
  ]);

  useEffect(() => {
    if (loginData) {
      // (loginData.oauth_access_token);
      AsyncStorage.setItem(constants.AUTH_TOKEN, loginData.oauth_access_token);

      SetLoginPath('App');
      setUser(loginData.uid);
      getProfile();
    }
  }, [SetLoginPath, getProfile, loginData, setUser]);

  useEffect(() => {
    if (errorData) {
      Show('INFORMATION_MODAL', { mode: 'error', message: errorData.msg });
    } else if (loginError) {
      Show('INFORMATION_MODAL', { mode: 'error', message: loginError.msg });
    }
  }, [errorData, loginError, Show]);

  const onPress = useCallback(() => {
    verify(mobile, otp);
  }, [mobile, otp, verify]);

  const theme = { roundness: 16, colors: { text: '#000' } };

  return (
    <ScrollView contentContainerStyle={styles.scroll}>
      <AppContainer style={styles.container}>
        <Logo style={styles.logo} />
        <View style={styles.title}>
          <Title style={styles.headline}>{i18n.t('otp_heading')}</Title>
          <Caption>{i18n.t('otp_caption')}</Caption>
        </View>

        <TextField
          value={otp}
          mode="mode"
          theme={theme}
          inputProps={{
            style: styles.textInput,
            underlineColor: 'transparent',
          }}
          onChangeText={setOTP}
          icon="cellphone-android"
          keyboardType="phone-pad"
          placeholder={i18n.t('otp_field_label')}
        />

        <Button
          mode="contained"
          onPress={onPress}
          loading={loading}
          theme={{
            roundness: 36,
            colors: { primary: '#40DCCD' },
            fonts: { medium: 'Poppins-Bold' },
          }}
          disabled={loading}>
          {i18n.t('verify_label')}
        </Button>

        <View style={styles.escoContainer}>
          <ESCOLogo style={styles.escoLogo} />
        </View>
      </AppContainer>
    </ScrollView>
  );
};

const mapStateToProps = createStructuredSelector({
  data: selectData,
  errorData: selectErrorData,
  loading: selectLoadingStatus,
  sentData: sendOTPSelectData,
  sentError: sendOTPSelectErrorData,
  sentLoading: sendOTPSelectLoadingStatus,

  loginData,
  loginError,
  loginLoading,
});

const mapDispatchToPrpos = {
  verify: verifyOTP,
  Show: showModal,
  SetLoginPath: setLoginPath,
  reset: otpReset,
  SendOTP: sendOTP,
  OTPReset: sendOTPReset,

  ShowModal: showLoader,
  HideModal: hideLoader,

  setProfileSuccess: getUserDetailsSuccess,
  setUser: setUserID,

  login,
  loginReset,
  getProfile: getUserDetails,
};

export default connect(mapStateToProps, mapDispatchToPrpos)(OTP);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: 16,
  },
  scroll: {
    flexGrow: 1,
  },
  title: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
  },
  logo: {
    width: 200,
    alignSelf: 'center',
  },
  headline: {
    marginTop: 20,
  },
  forgotButton: {
    alignItems: 'center',
    marginVertical: 8,
  },
  escoContainer: {
    flex: 1,
    marginVertical: 16,
    justifyContent: 'center',
    alignItems: 'center',
  },
  escoLogo: {},
  textInput: {
    marginBottom: 8,
    borderRadius: 16,
    fontFamily: 'Poppins-Medium',
  },
});

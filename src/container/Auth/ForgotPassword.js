import React, { useCallback, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { ScrollView, StyleSheet, View } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Caption, Button, Title, TouchableRipple } from 'react-native-paper';

import { i18n } from '@i18n';
import Logo from '@components/Logo';
import ESCOLogo from '@components/ESCOLogo';
import TextField from '@components/TextField';
import AppContainer from '@components/AppContainer';

import {
  forgotPassword,
  selectData,
  forgotPasswordReset,
  selectErrorData,
  selectLoadingStatus,
} from '../../redux/reducers/auth/forgotPassword';

import { showModal } from '@redux/reducers/modals';
import { setLoginPath } from '@redux/reducers/misc/setLoginPath';
import { showLoader, hideLoader } from '@redux/reducers/rootLoaderReducer';

const ForgotPassword = props => {
  const {
    loading,
    data,
    navigation,
    Show,
    Reset,
    DoForgotPassword,
    errorData,
  } = props;

  const [mobile, setMobile] = useState(null);
  const [password, setPassword] = useState(null);
  const [password1, setPassword1] = useState(null);

  const onPress = useCallback(() => {
    DoForgotPassword(mobile, password, password1);
  }, [DoForgotPassword, mobile, password, password1]);

  useEffect(() => {
    if (data) {
      Show('INFORMATION_MODAL', {
        mode: 'success',
        message: data.msg,
        onPress: () => {
          setMobile(null);
          setPassword(null);
          setPassword1(null);
        },
      });
    }
    return Reset;
  }, [data, Reset, Show, errorData]);

  useEffect(() => {
    if (errorData) {
      Show('INFORMATION_MODAL', { mode: 'error', message: errorData.msg });
    }
  }, [Show, errorData]);

  return (
    <ScrollView contentContainerStyle={styles.scroll}>
      <AppContainer style={styles.container}>
        <Logo style={styles.logo} />
        <Title style={styles.headline}>{i18n.t('forgot_heading')}</Title>

        <TextField
          value={mobile}
          style={styles.input}
          icon="cellphone-android"
          onChangeText={setMobile}
          placeholder={i18n.t('mobile_field_label')}
        />

        <TextField
          value={password}
          style={styles.input}
          icon="asterisk"
          onChangeText={setPassword}
          placeholder={i18n.t('password_field_label')}
        />

        <TextField
          value={password1}
          style={styles.input}
          icon="asterisk"
          onChangeText={setPassword1}
          placeholder={i18n.t('renter_password_field_label')}
        />

        <Button
          mode="contained"
          onPress={onPress}
          loading={loading}
          disabled={loading}>
          {i18n.t('reset_label')}
        </Button>

        <View style={styles.escoContainer}>
          <ESCOLogo style={styles.escoLogo} />
        </View>
      </AppContainer>
    </ScrollView>
  );
};

const mapStateToProps = createStructuredSelector({
  data: selectData,
  errorData: selectErrorData,
  loading: selectLoadingStatus,
});

const mapDispatchToPrpos = {
  DoForgotPassword: forgotPassword,
  Show: showModal,
  Reset: forgotPasswordReset,
};

export default connect(mapStateToProps, mapDispatchToPrpos)(ForgotPassword);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: 16,
  },
  scroll: {
    flexGrow: 1,
  },
  logo: {
    width: 200,
    alignSelf: 'center',
  },
  headline: {
    marginTop: 50,
    marginBottom: 16,
  },
  forgotButton: {
    alignItems: 'center',
    marginVertical: 8,
  },
  escoContainer: {
    flex: 1,
    marginVertical: 16,
    justifyContent: 'center',
    alignItems: 'center',
  },
  escoLogo: {},
});

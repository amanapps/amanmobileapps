export default {
  // misc strings
  submit_label: 'يقدم',
  ok_label: 'نعم',
  cancel_label: 'يلغي',
  yes_label: 'نعم',
  no_label: 'لا',
  oh_snap: 'أوه المفاجئة!',
  that_great: 'رائعة',
  dismiss: 'رفض',
  verify_label: 'التحقق',
  reset_label: 'إعادة ضبط',
  next_label: 'التالي',

  // Login strings
  login_heading: 'دعونا تسجيل الدخول.',
  mobile_field_label: 'أدخل رقم هاتفك المحمول',
  password_field_label: 'ادخل رقمك السري',
  login_button_label: 'تسجيل الدخول',
  forgot_password_label: 'هل نسيت كلمة السر ؟',
  registration_label: 'انشئ حساب',
  // forgot password strings
  forgot_heading: 'هل نسيت كلمة السر',
  renter_password_field_label: 'اعادة ادخال كلمة السر',
  // otp string
  otp_heading: 'تحقق',
  otp_caption: 'سوف تحصل على OTP عبر رسالة نصية قصيرة',
  otp_field_label: 'أدخل OTP الخاص بك',
  // registration strings
  registration_heading: 'معلومات شخصية',
  registration_caption:
    'يرجى إدخال معلوماتك والانتقال إلى الخطوة التالية حتى نتمكن من إنشاء حساباتك.',
  registration_full_name_label: 'الاسم بالكامل',
  registration_full_name_placholder: 'أدخل أسمك',
  registration_mobile_number_label: 'هاتفك النقال',
  registration_mobile_number_placeholder: 'أدخل رقم الهاتف المحمول',
  registration_password_label: 'Password',
  registration_password_placeholder: 'Enter password',
  registration_dob_label: 'أدخل رقم الهاتف المحمول',
  registration_gender_label: 'جنس تذكير أو تأنيث',
  registration_gender_option_male_label: 'ذكر',
  registration_gender_option_female_label: 'أنثى',
  registration_full_address_label: 'العنوان الكامل',
  registration_street_one_label: 'شارع 1',
  registration_street_two_label: 'شارع 2',
  registration_defined_quater_label: 'تحديد الربع',
  registration_state_label: 'الولاية / المقاطعة',
  // service type form
  service_type_label: 'نوع الخدمة',
  service_type_caption:
    'يرجى تحديد الخدمة للاطلاع على التفاصيل والانتقال إلى الخطوة التالية حتى نتمكن من إنشاء حساباتك.',
  // carrier infroamtion
  carrier_info_label: 'معلومات الناقل',
  carrier_registration_label: 'نوع التسجيل*',
  carrier_union_label: 'اتحاد',
  carrier_carrier_label: 'الناقل',
  carrier_profession_label: 'مهنة',
  // membership package
  membership_package_label: 'باقات العضوية',
  member_membership_label: 'باقة العضوية',
  member_bank_label: 'مصرف',
  // health declaration form
  health_declaration_label: 'إعلان صحي',
  declaration_caption:
    'إذا سبق الحد الأقصى للأشخاص المذكورين في هذا النموذج من تعرضه للإصابة أو العلاج أو الخضوع لعملية جراحية بسبب أي من الأمراض المذكورة أدناه خلال السنوات العشر الماضية ، يرجى وضع علامة () في المربع المخصص للحالة الصحية.',

  // Home page
  new_claim_label: 'مطالبة جديدة',
  new_request_label: 'طلب جديد',
  welcome_label: 'أهلا بك {{name}},',
};

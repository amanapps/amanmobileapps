export default {
  // misc strings
  submit_label: 'Submit',
  ok_label: 'OK',
  cancel_label: 'Cancel',
  yes_label: 'Yes',
  no_label: 'No',
  oh_snap: 'Oh snap!',
  that_great: 'Great',
  dismiss: 'Dismiss',
  verify_label: 'Verify',
  reset_label: 'Reset',
  next_label: 'Next',
  no_data_found: 'No Data Found',
  sign_up: 'Sign up',
  sign_out: 'Sign Out',
  general_ads: 'General Ads',
  // splash_screen
  splash_heading: 'Platform',
  splash_description: 'Inspection, Committees Confirmation Application',
  splash_partnership:
    'In partnership with the General Federation for trade unions',
  splash_implementation: 'From the implementation:',
  splash_company: 'Al Bahja Land Company',
  splash_powered_by: 'Powered By',
  // Login strings
  login_heading: 'Welcome',
  mobile_field_label: 'Enter your mobile number',
  password_field_label: 'Enter your password',
  login_button_label: 'Sign In',
  forgot_password_label: 'Forgot Password?',
  registration_label: 'Register Here',
  dont_have_account: "Don't have account?",
  // forgot password strings
  forgot_heading: 'Forgot Password',
  renter_password_field_label: 'Re-enter password',
  // otp verification
  otp_heading: 'Verification',
  otp_caption: 'You will get a OTP via SMS',
  otp_field_label: 'Enter your OTP',
  // registration strings
  registration_heading: 'Sign Up',
  registration_caption: 'Add your details to sign up.',
  registration_full_name_label: 'Full Name',
  registration_full_name_placholder: 'Enter your name',
  registration_mobile_number_label: 'Your Mobile',
  registration_mobile_number_placeholder: 'Enter mobile number',
  registration_password_label: 'Password',
  registration_password_placeholder: 'Enter password',
  registration_dob_label: 'Birth date',
  registration_gender_label: 'Gender',
  registration_gender_option_male_label: 'Male',
  registration_gender_option_female_label: 'Female',
  registration_full_address_label: 'Full address',
  registration_street_one_label: 'Street1',
  registration_street_two_label: 'Street2',
  registration_defined_quater_label: 'Defined Quater',
  registration_state_label: 'State/Province',

  // service type form
  service_type_label: 'Service Type',
  service_type_caption:
    'Please select service to see details and proceed to the next step so we can build your accounts.',
  // carrier infroamtion
  carrier_info_label: 'Carrier Information',
  carrier_registration_label: 'Registration Type*',
  carrier_union_label: 'Union',
  carrier_carrier_label: 'Carrier',
  carrier_profession_label: 'Profession',
  // membership package
  membership_package_label: 'Membership Packages',
  member_membership_label: 'Membership Package',
  member_bank_label: 'Bank',
  // health declaration form
  health_declaration_label: 'Health Declaration',
  declaration_caption:
    'If the limit preceded the persons listed on this form that he was injured, treated, or underwent surgery due to any of the diseases listed below during the last ten years, please put a tick () in the box designated for health status.',

  // Home page
  home_hi: 'Hi',
  new_claim_label: 'New Claim',
  new_request_label: 'New Request',
  welcome_label: 'Welcome {{name}},',

  // search
  search_active: 'Active',
  search_expired: 'Expired',

  activity_date: 'Activity Date',
  name_of_the_employee: 'Name of the employees',
  visit_report: 'Visit Report',
  type_of_work: 'Type of work (agri, Health etc.)',
  specialization: 'Specialization',
  shop_name: 'Shop Name',
  location: 'Visiting site location',
  commite: 'Commitee ID',

  owner_name: 'Owner Name',
  owner_mobile: 'owner Mobile',
  Website: 'Website',
  business_phone: 'Business Phone',
  street_2: 'Street 2',
  street: 'Street',
  City: 'City',
  State: 'State',
  Union: 'Related Union',
  Name: 'Name',
  business_type: 'Business Type',

  invoice_status: 'Status: {{status}}',
  invoice_date: 'Date {{invoice_date}}',
  amount_total: 'Amount: {{amount_total}}',

  committee_member_name: 'Committee Member Nname',
  badge: 'Badge No.',
  book: 'Book No.',
  expiry: 'Expiry Date',
  not_found: 'Not Found',
  search_not_found:
    'Search did not found the entered value, Please correct your entries',

  contact_us: 'Contact Us',

  message: 'Message',
  edit_profile: 'Edit Profile',
  update: 'Update',
  registration_business_type_label: 'Business Type',
};

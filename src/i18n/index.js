import i18n from 'i18n-js';
import * as RNLocalize from 'react-native-localize';

import { en, ara as ar } from '@locale/supportedLanguages';

console.log(
  RNLocalize.getLocales(),
  RNLocalize.findBestAvailableLanguage(RNLocalize.getLocales()),
);

i18n.fallbacks = true;
i18n.translations = { en, ar };

if (__DEV__) {
  i18n.locale = 'en';
} else {
  i18n.locale = RNLocalize.getLocales()[0].languageCode;
}

i18n.locale = RNLocalize.getLocales()[0].languageCode;

export { i18n };

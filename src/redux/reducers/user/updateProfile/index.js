import reducerRegistry from '@redux/core/reducerRegistry';
import createActionName from '@redux/core/createActionName';

import { apiDispatcher } from '@api';
import { getUserDetails } from '../getDetails';

const initialState = {
  loading: false,
  error: false,
  data: null,
  errorData: {},
};

const parent = 'USER';
export const reducerName = `${parent}-updateDetails`;

// SELECTORS //
export const selectData = state =>
  state[reducerName] ? state[reducerName].data : initialState.data;

export const selectErrorStatus = state =>
  state[reducerName] ? state[reducerName].error : initialState.error;
export const selectErrorData = state =>
  state[reducerName] ? state[reducerName].errorData : initialState.errorData;

export const selectLoadingStatus = state =>
  state[reducerName] ? state[reducerName].loading : initialState.loading;

// ACTIONS //
export const USER_DETAILS_LOADING = createActionName(reducerName, 'LOADING');
export const USER_DETAILS_SUCCESS = createActionName(reducerName, 'SUCCESS');
export const USER_DETAILS_ERROR = createActionName(reducerName, 'ERROR');
export const RESET = createActionName(reducerName, 'RESET');

// ACTION CREATORS //
export const updateProfileLoading = () => {
  return {
    type: USER_DETAILS_LOADING,
  };
};

export const updateProfileSuccess = payload => {
  return {
    type: USER_DETAILS_SUCCESS,
    payload,
  };
};

export const updateProfileError = error => {
  return {
    type: USER_DETAILS_ERROR,
    error,
  };
};

export const reset = () => {
  return {
    type: RESET,
  };
};

// ASYNC ACTIONS //
export const updateProfile = post => {
  return dispatch => {
    dispatch(updateProfileLoading());
    apiDispatcher('update_profile', post).then(response => {
      if (response.status > 0 && response.status < 400) {
        dispatch(updateProfileSuccess(response.data));
      } else {
        dispatch(updateProfileError(response.data));
      }
    });
  };
};

// REDUCER //
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case USER_DETAILS_LOADING:
      return {
        ...state,
        loading: true,
      };
    case USER_DETAILS_SUCCESS:
      return {
        ...state,
        error: false,
        loading: false,
        data: action.payload,
      };
    case USER_DETAILS_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        errorData: action.error,
      };
    case RESET:
      return initialState;
    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

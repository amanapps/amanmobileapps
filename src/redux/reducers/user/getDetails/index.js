import reducerRegistry from '@redux/core/reducerRegistry';
import createActionName from '@redux/core/createActionName';

import { apiDispatcher } from '@api';

const initialState = {
  loading: false,
  error: false,
  data: {},
  errorData: {},
};

const parent = 'USER';
export const reducerName = `${parent}-getDetails`;

// SELECTORS //
export const selectData = state =>
  state[reducerName] ? state[reducerName].data : initialState.data;

export const selectErrorStatus = state =>
  state[reducerName] ? state[reducerName].error : initialState.error;
export const selectErrorData = state =>
  state[reducerName] ? state[reducerName].errorData : initialState.errorData;

export const selectLoadingStatus = state =>
  state[reducerName] ? state[reducerName].loading : initialState.loading;

// ACTIONS //
export const USER_DETAILS_LOADING = createActionName(reducerName, 'LOADING');
export const USER_DETAILS_SUCCESS = createActionName(reducerName, 'SUCCESS');
export const USER_DETAILS_ERROR = createActionName(reducerName, 'ERROR');
export const RESET = createActionName(reducerName, 'RESET');

// ACTION CREATORS //
export const getUserDetailsLoading = () => {
  return {
    type: USER_DETAILS_LOADING,
  };
};

export const getUserDetailsSuccess = payload => {
  return {
    type: USER_DETAILS_SUCCESS,
    payload,
  };
};

export const getUserDetailsError = error => {
  return {
    type: USER_DETAILS_ERROR,
    error,
  };
};

export const reset = () => {
  return {
    type: RESET,
  };
};

// ASYNC ACTIONS //
export const getUserDetails = (merchant = true) => {
  return dispatch => {
    dispatch(getUserDetailsLoading());
    apiDispatcher(merchant ? 'merchant_profile' : 'profile').then(response => {
      if (response.status > 0 && response.status < 400) {
        dispatch(getUserDetailsSuccess(response.data.data));
      } else {
        dispatch(getUserDetailsError(response.data));
      }
    });
  };
};

// REDUCER //
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case USER_DETAILS_LOADING:
      return {
        ...state,
        loading: true,
      };
    case USER_DETAILS_SUCCESS:
      return {
        ...state,
        error: false,
        loading: false,
        data: action.payload,
      };
    case USER_DETAILS_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        errorData: action.error,
      };
    case RESET:
      return initialState;
    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

import reducerRegistry from '@redux/core/reducerRegistry';
import createActionName from '@redux/core/createActionName';

const initialState = {
  type: null,
  props: {},
};

const reducerName = 'modal';

// SELECTORS //
export const selectModalType = (state) => state[reducerName].type;
export const selectModalProps = (state) => state[reducerName].props;

// ACTIONS //
export const SHOW_MODAL = createActionName(reducerName, 'SHOW_MODAL');
export const HIDE_MODAL = createActionName(reducerName, 'HIDE_MODAL');

// ACTION CREATORS //
export const showModal = (type, props) => ({
  type: SHOW_MODAL,
  payload: {
    type,
    props,
  },
});

export const hideModal = (type, props) => ({
  type: HIDE_MODAL,
  payload: {
    type,
    props,
  },
});

// REDUCER //
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SHOW_MODAL:
      return {
        ...state,
        type: action.payload.type,
        props: { ...action.payload.props, show: true },
      };
    case HIDE_MODAL:
      return {
        ...state,
        type: action.payload.type,
        props: { ...action.payload.props, show: false },
      };
    case '@@router/LOCATION_CHANGE':
      return initialState;
    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

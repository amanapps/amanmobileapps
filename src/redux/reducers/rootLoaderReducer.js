import reducerRegistry from '@redux/core/reducerRegistry';
import createActionName from '@redux/core/createActionName';

const initialState = {
  type: null,
  props: {},
};

const reducerName = 'loader';

// SELECTORS //
export const selectType = (state) => state[reducerName].type;
export const selectProps = (state) => state[reducerName].props;

// ACTIONS //
export const SHOW_LOADER = createActionName(reducerName, 'SHOW_LOADER');
export const HIDE_LOADER = createActionName(reducerName, 'HIDE_LOADER');

// ACTION CREATORS //
export const showLoader = (type, props) => ({
  type: SHOW_LOADER,
  payload: {
    type,
    props,
  },
});

export const hideLoader = (type, props) => ({
  type: HIDE_LOADER,
  payload: {
    type,
    props,
  },
});

// REDUCER //
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SHOW_LOADER:
      return {
        ...state,
        type: action.payload.type,
        props: { ...action.payload.props, show: true },
      };
    case HIDE_LOADER:
      return {
        ...state,
        type: action.payload.type,
        props: { ...action.payload.props, show: false },
      };
    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

import reducerRegistry from '@redux/core/reducerRegistry';
import createActionName from '@redux/core/createActionName';

import { apiDispatcher } from '@api';

const initialState = {
  loading: false,
  error: false,
  data: [],
  errorData: {},
};

const parent = 'Transaction';
export const reducerName = `${parent}-invoices`;

// SELECTORS //
export const selectData = state =>
  state[reducerName] ? state[reducerName].data : initialState.data;

export const selectErrorStatus = state =>
  state[reducerName] ? state[reducerName].error : initialState.error;
export const selectErrorData = state =>
  state[reducerName] ? state[reducerName].errorData : initialState.errorData;

export const selectLoadingStatus = state =>
  state[reducerName] ? state[reducerName].loading : initialState.loading;

// ACTIONS //
export const HISTORY_INVOICE_LOADING = createActionName(reducerName, 'LOADING');
export const HISTORY_INVOICE_SUCCESS = createActionName(reducerName, 'SUCCESS');
export const HISTORY_INVOICE_ERROR = createActionName(reducerName, 'ERROR');
export const RESET = createActionName(reducerName, 'RESET');

// ACTION CREATORS //
export const getTransactionInvoiceLoading = () => {
  return {
    type: HISTORY_INVOICE_LOADING,
  };
};

export const getTransactionInvoiceSuccess = payload => {
  return {
    type: HISTORY_INVOICE_SUCCESS,
    payload,
  };
};

export const getTransactionInvoiceError = error => {
  return {
    type: HISTORY_INVOICE_ERROR,
    error,
  };
};

export const reset = () => {
  return {
    type: RESET,
  };
};

// ASYNC ACTIONS //
export const getTransactionInvoices = () => {
  return dispatch => {
    dispatch(getTransactionInvoiceLoading());
    apiDispatcher('transaction_invoice').then(response => {
      if (response.status > 0 && response.status < 400) {
        dispatch(getTransactionInvoiceSuccess(response.data.data.invoices));
      } else {
        dispatch(getTransactionInvoiceError(response.data));
      }
    });
  };
};

// REDUCER //
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case HISTORY_INVOICE_LOADING:
      return {
        ...state,
        loading: true,
      };
    case HISTORY_INVOICE_SUCCESS:
      return {
        ...state,
        error: false,
        loading: false,
        data: action.payload,
      };
    case HISTORY_INVOICE_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        errorData: action.error,
      };
    case RESET:
      return initialState;
    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

import reducerRegistry from '@redux/core/reducerRegistry';
import createActionName from '@redux/core/createActionName';

import { apiDispatcher } from '@api';

const initialState = {
  loading: false,
  error: false,
  data: {},
  errorData: null,
};

const parent = 'COMMITTE';
export const reducerName = `${parent}-activity_log`;

// SELECTORS //
export const selectData = state =>
  state[reducerName] ? state[reducerName].data : initialState.data;

export const selectErrorStatus = state =>
  state[reducerName] ? state[reducerName].error : initialState.error;
export const selectErrorData = state =>
  state[reducerName] ? state[reducerName].errorData : initialState.errorData;

export const selectLoadingStatus = state =>
  state[reducerName] ? state[reducerName].loading : initialState.loading;

// ACTIONS //
export const SET_ACTIVITY_LOG_LOADING = createActionName(
  reducerName,
  'LOADING',
);
export const SET_ACTIVITY_LOG_SUCCESS = createActionName(
  reducerName,
  'SUCCESS',
);
export const SET_ACTIVITY_LOG_ERROR = createActionName(reducerName, 'ERROR');
export const RESET = createActionName(reducerName, 'RESET');

// ACTION CREATORS //
export const setActivityLogLoading = () => {
  return {
    type: SET_ACTIVITY_LOG_LOADING,
  };
};

export const setActivityLogSuccess = payload => {
  return {
    type: SET_ACTIVITY_LOG_SUCCESS,
    payload,
  };
};

export const setActivityLogError = error => {
  return {
    type: SET_ACTIVITY_LOG_ERROR,
    error,
  };
};

export const reset = () => {
  return {
    type: RESET,
  };
};

// ASYNC ACTIONS //
export const setActivityLog = postData => {
  return dispatch => {
    dispatch(setActivityLogLoading());
    apiDispatcher('log_activity', postData).then(response => {
      if (response.status > 0 && response.status < 400) {
        dispatch(setActivityLogSuccess(response.data));
      } else {
        dispatch(setActivityLogError(response.data));
      }
    });
  };
};

// REDUCER //
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SET_ACTIVITY_LOG_LOADING:
      return {
        ...state,
        loading: true,
      };
    case SET_ACTIVITY_LOG_SUCCESS:
      return {
        ...state,
        error: false,
        loading: false,
        data: action.payload,
      };
    case SET_ACTIVITY_LOG_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        errorData: action.error,
      };
    case RESET:
      return initialState;
    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

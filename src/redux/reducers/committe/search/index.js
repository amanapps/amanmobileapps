import reducerRegistry from '@redux/core/reducerRegistry';
import createActionName from '@redux/core/createActionName';

import { apiDispatcher } from '@api';

const initialState = {
  loading: false,
  error: false,
  data: {},
  searched: false,
  errorData: {},
};

const parent = 'COMMITTE';
export const reducerName = `${parent}-search`;

// SELECTORS //
export const selectData = state =>
  state[reducerName] ? state[reducerName].data : initialState.data;

export const selectErrorStatus = state =>
  state[reducerName] ? state[reducerName].error : initialState.error;
export const selectErrorData = state =>
  state[reducerName] ? state[reducerName].errorData : initialState.errorData;

export const selectLoadingStatus = state =>
  state[reducerName] ? state[reducerName].loading : initialState.loading;

export const selectIsSearched = state =>
  state[reducerName] ? state[reducerName].searched : initialState.searched;

// ACTIONS //
export const MEMBER_SEARCH_LOADING = createActionName(reducerName, 'LOADING');
export const MEMBER_SEARCH_SUCCESS = createActionName(reducerName, 'SUCCESS');
export const MEMBER_SEARCH_ERROR = createActionName(reducerName, 'ERROR');
export const RESET = createActionName(reducerName, 'RESET');

// ACTION CREATORS //
export const getMemberSearchLoading = () => {
  return {
    type: MEMBER_SEARCH_LOADING,
  };
};

export const getMemberSearchSuccess = payload => {
  return {
    type: MEMBER_SEARCH_SUCCESS,
    payload,
  };
};

export const getMemberSearchError = error => {
  return {
    type: MEMBER_SEARCH_ERROR,
    error,
  };
};

export const reset = () => {
  return {
    type: RESET,
  };
};

// ASYNC ACTIONS //
export const getMemberSearch = value => {
  return dispatch => {
    dispatch(getMemberSearchLoading());
    apiDispatcher('committee_member_search', { value }).then(response => {
      if (response.status > 0 && response.status < 400) {
        dispatch(getMemberSearchSuccess(response.data.Response));
      } else {
        dispatch(getMemberSearchError(response.data));
      }
    });
  };
};

// REDUCER //
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case MEMBER_SEARCH_LOADING:
      return {
        ...state,
        loading: true,
        searched: true,
        data: {},
      };
    case MEMBER_SEARCH_SUCCESS:
      return {
        ...state,
        error: false,
        loading: false,
        data: action.payload,
      };
    case MEMBER_SEARCH_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        errorData: action.error,
      };
    case RESET:
      return initialState;
    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

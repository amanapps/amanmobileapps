import reducerRegistry from '@redux/core/reducerRegistry';
import createActionName from '@redux/core/createActionName';

const initialState = {
  data: '',
};

const parent = 'Misc';
export const reducerName = `${parent}-setLoginPath`;

// SELECTORS //
export const selectData = state =>
  state[reducerName] ? state[reducerName].data : initialState.data;

// ACTIONS //
export const SET_LOGIN_PATH = createActionName(reducerName, 'SUCCESS');

// ACTION CREATORS //
export const setLoginPath = payload => {
  return {
    type: SET_LOGIN_PATH,
    payload,
  };
};

// REDUCER //
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SET_LOGIN_PATH:
      return {
        ...state,
        data: action.payload,
      };
    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

import { apiDispatcher } from '@api';

import reducerRegistry from '@redux/core/reducerRegistry';
import createActionName from '@redux/core/createActionName';

const initialState = {
  loading: false,
  error: false,
  errorData: null,
  data: [],
};

const parent = 'MISC';
export const reducerName = `${parent}-country-state`;

// SELECTORS //
export const selectData = state =>
  state[reducerName] ? state[reducerName].data : initialState.data;
export const selectErrorStatus = state =>
  state[reducerName] ? state[reducerName].error : initialState.error;
export const selectErrorData = state =>
  state[reducerName] ? state[reducerName].errorData : initialState.errorData;
export const selectLoadingStatus = state =>
  state[reducerName] ? state[reducerName].loading : initialState.loading;

// ACTIONS //
export const COUNTRY_STATE_ERROR = createActionName(reducerName, 'ERROR');
export const COUNTRY_STATE_LOADING = createActionName(reducerName, 'LOADING');
export const COUNTRY_STATE_SUCCESS = createActionName(reducerName, 'SUCCESS');
export const COUNTRY_STATE_RESET = createActionName(reducerName, 'RESET');

// ACTION CREATOR //
export const getCountryStateLoading = () => ({ type: COUNTRY_STATE_LOADING });

export const getCountryStateError = payload => ({
  type: COUNTRY_STATE_ERROR,
  payload,
});

export const getCountryStateSuccess = payload => ({
  type: COUNTRY_STATE_SUCCESS,
  payload,
});

export const getCountryStateReset = () => ({ type: COUNTRY_STATE_RESET });

export const getCountryStates = (country_id = 106) => {
  return dispatch => {
    dispatch(getCountryStateLoading());
    apiDispatcher('fetch_states', { country_id }).then(response => {
      const { status, data } = response;
      if (status > 0 && status < 400) {
        // set current data
        dispatch(
          getCountryStateSuccess(
            data.states.map(item => ({
              ...item,
              label: item.name,
              value: item.id,
            })),
          ),
        );
      } else {
        dispatch(getCountryStateError(data));
      }
    });
  };
};

// REDUCER //
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case COUNTRY_STATE_LOADING:
      return {
        ...state,
        loading: true,
        error: false,
      };
    case COUNTRY_STATE_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
        data: action.payload,
      };
    case COUNTRY_STATE_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        errorData: action.payload,
      };
    case COUNTRY_STATE_RESET:
      return initialState;

    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

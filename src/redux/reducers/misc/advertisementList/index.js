import { apiDispatcher } from '@api';

import reducerRegistry from '@redux/core/reducerRegistry';
import createActionName from '@redux/core/createActionName';

const initialState = {
  loading: false,
  error: false,
  errorData: null,
  data: {},
};

const parent = 'MISC';
export const reducerName = `${parent}-advertisement-list`;

// SELECTORS //
export const selectData = state =>
  state[reducerName] ? state[reducerName].data : initialState.data;
export const selectErrorStatus = state =>
  state[reducerName] ? state[reducerName].error : initialState.error;
export const selectErrorData = state =>
  state[reducerName] ? state[reducerName].errorData : initialState.errorData;
export const selectLoadingStatus = state =>
  state[reducerName] ? state[reducerName].loading : initialState.loading;

// ACTIONS //
export const GET_ADS_LIST_ERROR = createActionName(reducerName, 'ERROR');
export const GET_ADS_LIST_LOADING = createActionName(reducerName, 'LOADING');
export const GET_ADS_LIST_SUCCESS = createActionName(reducerName, 'SUCCESS');
export const GET_ADS_LIST_RESET = createActionName(reducerName, 'RESET');

// ACTION CREATOR //
export const getAdsListLoading = () => ({ type: GET_ADS_LIST_LOADING });

export const getAdsListError = payload => ({
  type: GET_ADS_LIST_ERROR,
  payload,
});

export const getAdsListSuccess = payload => ({
  type: GET_ADS_LIST_SUCCESS,
  payload,
});

export const getAdsListReset = () => ({ type: GET_ADS_LIST_RESET });

export const getAdsList = () => {
  return dispatch => {
    dispatch(getAdsListLoading());
    apiDispatcher('advertisement_list').then(response => {
      const { status, data } = response;
      if (status > 0 && status < 400) {
        // set current data
        dispatch(getAdsListSuccess(data));
      } else {
        dispatch(getAdsListError(data));
      }
    });
  };
};

// REDUCER //
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case GET_ADS_LIST_LOADING:
      return {
        ...state,
        loading: true,
        error: false,
      };
    case GET_ADS_LIST_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
        data: action.payload,
      };
    case GET_ADS_LIST_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        errorData: action.payload,
      };
    case GET_ADS_LIST_RESET:
      return initialState;

    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

import createActionName from '@redux/core/createActionName';
import reducerRegistry from '@redux/core/reducerRegistry';

const parent = 'MISC';
export const reducerName = `${parent}-logout`;

// ACTIONS //
export const LOGOUT_SUCCESS = createActionName(reducerName, 'LOGOUT');

export const logoutSuccess = payload => {
  return {
    type: LOGOUT_SUCCESS,
    payload,
  };
};

// REDUCER //
export default function reducer(state = {}, action = {}) {
  switch (action.type) {
    case LOGOUT_SUCCESS:
      return state;

    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

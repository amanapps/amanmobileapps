import reducerRegistry from '@redux/core/reducerRegistry';
import createActionName from '@redux/core/createActionName';

const initialState = {
  type: null,
  props: {},
};

const reducerName = 'bottomSheet';

// SELECTORS //
export const selectBottomSheetProps = (state) => state[reducerName].props;

// ACTIONS //
export const SHOW_SHEET = createActionName(reducerName, 'SHOW_SHEET');
export const HIDE_SHEET = createActionName(reducerName, 'HIDE_SHEET');

// ACTION CREATORS //
export const showSheet = (props) => ({
  type: SHOW_SHEET,
  payload: {
    props,
  },
});

export const hideSheet = (props) => ({
  type: HIDE_SHEET,
  payload: {
    props,
  },
});

// REDUCER //
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SHOW_SHEET:
      return {
        ...state,
        props: { ...action.payload.props, show: true },
      };
    case HIDE_SHEET:
      return {
        ...state,
        props: { ...action.payload.props, show: false },
      };
    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

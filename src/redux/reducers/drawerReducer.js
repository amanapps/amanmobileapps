import reducerRegistry from '@redux/core/reducerRegistry';
import createActionName from '@redux/core/createActionName';

const initialState = {
  type: null,
  props: {},
};

const reducerName = 'drawer';

// SELECTORS //
export const selectBottomSheetProps = (state) => state[reducerName].props;

// ACTIONS //
export const SHOW_DRAWER = createActionName(reducerName, 'SHOW_DRAWER');
export const HIDE_DRAWER = createActionName(reducerName, 'HIDE_DRAWER');

// ACTION CREATORS //
export const showDrawer = (props) => ({
  type: SHOW_DRAWER,
  payload: {
    props,
  },
});

export const hidedrawer = (props) => ({
  type: HIDE_DRAWER,
  payload: {
    props,
  },
});

// REDUCER //
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SHOW_DRAWER:
      return {
        ...state,
        props: { ...action.payload.props, show: true },
      };
    case HIDE_DRAWER:
      return {
        ...state,
        props: { ...action.payload.props, show: false },
      };
    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

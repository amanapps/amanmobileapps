import { apiDispatcher } from '@api';

import reducerRegistry from '@redux/core/reducerRegistry';
import createActionName from '@redux/core/createActionName';

const initialState = {
  loading: false,
  error: false,
  errorData: null,
  data: null,
};

const parent = 'MISC';
export const reducerName = `${parent}-service_type`;

// SELECTORS //
export const selectData = state =>
  state[reducerName] ? state[reducerName].data : initialState.data;
export const selectErrorStatus = state =>
  state[reducerName] ? state[reducerName].error : initialState.error;
export const selectErrorData = state =>
  state[reducerName] ? state[reducerName].errorData : initialState.errorData;
export const selectLoadingStatus = state =>
  state[reducerName] ? state[reducerName].loading : initialState.loading;

// ACTIONS //
export const SERVICE_TYPE_ERROR = createActionName(reducerName, 'ERROR');
export const SERVICE_TYPE_LOADING = createActionName(reducerName, 'LOADING');
export const SERVICE_TYPE_SUCCESS = createActionName(reducerName, 'SUCCESS');
export const SERVICE_TYPE_RESET = createActionName(reducerName, 'RESET');

// ACTION CREATOR //
export const serviceTypeLoading = () => ({ type: SERVICE_TYPE_LOADING });

export const serviceTypeError = payload => ({
  type: SERVICE_TYPE_ERROR,
  payload,
});

export const serviceTypeSuccess = payload => ({
  type: SERVICE_TYPE_SUCCESS,
  payload,
});

export const serviceTypeReset = () => ({ type: SERVICE_TYPE_RESET });

export const getServiceTypes = () => {
  return dispatch => {
    dispatch(serviceTypeLoading());
    apiDispatcher('fetch_service_type').then(response => {
      const { status, data } = response;
      if (status > 0 && status < 400) {
        // set current data
        dispatch(serviceTypeSuccess(data));
      } else {
        dispatch(serviceTypeError(data));
      }
    });
  };
};

// REDUCER //
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SERVICE_TYPE_LOADING:
      return {
        ...state,
        loading: true,
        error: false,
      };
    case SERVICE_TYPE_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
        data: action.payload,
      };
    case SERVICE_TYPE_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        errorData: action.payload,
      };
    case SERVICE_TYPE_RESET:
      return initialState;

    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

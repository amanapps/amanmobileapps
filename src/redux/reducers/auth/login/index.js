import { apiDispatcher } from '@api';

import reducerRegistry from '@redux/core/reducerRegistry';
import createActionName from '@redux/core/createActionName';
import { setUserID } from '../setUserID';
import { setRefreshToken } from '../setRefreshToken';
import { setAuthToken } from '../setAuthToken';

const initialState = {
  loading: false,
  error: false,
  errorData: null,
  data: null,
};

const parent = 'AUTH';
export const reducerName = `${parent}-login`;

// SELECTORS //
export const selectData = state =>
  state[reducerName] ? state[reducerName].data : initialState.data;
export const selectErrorStatus = state =>
  state[reducerName] ? state[reducerName].error : initialState.error;
export const selectErrorData = state =>
  state[reducerName] ? state[reducerName].errorData : initialState.errorData;
export const selectLoadingStatus = state =>
  state[reducerName] ? state[reducerName].loading : initialState.loading;

// ACTIONS //
export const LOGIN_ERROR = createActionName(reducerName, 'ERROR');
export const LOGIN_LOADING = createActionName(reducerName, 'LOADING');
export const LOGIN_SUCCESS = createActionName(reducerName, 'SUCCESS');
export const LOGIN_RESET = createActionName(reducerName, 'RESET');

// ACTION CREATOR //
export const loginLoading = () => ({ type: LOGIN_LOADING });

export const loginError = payload => ({ type: LOGIN_ERROR, payload });

export const loginSuccess = payload => ({ type: LOGIN_SUCCESS, payload });

export const loginReset = () => ({ type: LOGIN_RESET });

export const login = (mobile, password) => {
  return dispatch => {
    dispatch(loginLoading());
    apiDispatcher('login', { password, mobile }).then(response => {
      const { status, data } = response;
      if (status > 0 && status < 400) {
        // set current data
        dispatch(loginSuccess(data));

        // set user id
        // dispatch(setUserID(data.data.user.id));

        // // set auth and refresh token
        // dispatch(setRefreshToken(data.data.refresh));
        dispatch(setAuthToken(data.oauth_access_token));
      } else {
        dispatch(loginError(data));
      }
    });
  };
};

// REDUCER //
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOGIN_LOADING:
      return {
        ...state,
        loading: true,
        error: false,
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
        data: action.payload,
      };
    case LOGIN_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        errorData: action.payload,
      };
    case LOGIN_RESET:
      return initialState;

    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

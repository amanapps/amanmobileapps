import { apiDispatcher } from '@api';

import reducerRegistry from '@redux/core/reducerRegistry';
import createActionName from '@redux/core/createActionName';

const initialState = {
  loading: false,
  error: false,
  errorData: null,
  data: null,
};

const parent = 'AUTH';
export const reducerName = `${parent}-OTP-send`;

// SELECTORS //
export const selectData = state =>
  state[reducerName] ? state[reducerName].data : initialState.data;
export const selectErrorStatus = state =>
  state[reducerName] ? state[reducerName].error : initialState.error;
export const selectErrorData = state =>
  state[reducerName] ? state[reducerName].errorData : initialState.errorData;
export const selectLoadingStatus = state =>
  state[reducerName] ? state[reducerName].loading : initialState.loading;

// ACTIONS //
export const SEND_OTP_ERROR = createActionName(reducerName, 'ERROR');
export const SEND_OTP_LOADING = createActionName(reducerName, 'LOADING');
export const SEND_OTP_SUCCESS = createActionName(reducerName, 'SUCCESS');
export const SEND_OTP_RESET = createActionName(reducerName, 'RESET');

// ACTION CREATOR //
export const otpLoading = () => ({ type: SEND_OTP_LOADING });

export const otpError = payload => ({ type: SEND_OTP_ERROR, payload });

export const otpSuccess = payload => ({ type: SEND_OTP_SUCCESS, payload });

export const sendOTPReset = () => ({ type: SEND_OTP_RESET });

export const sendOTP = Mobile => {
  return dispatch => {
    dispatch(otpLoading());
    apiDispatcher('send_otp', { Mobile }).then(response => {
      const { status, data } = response;
      if (status > 0 && status < 400) {
        dispatch(otpSuccess(data));
      } else {
        dispatch(otpError(data));
      }
    });
  };
};

// REDUCER //
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SEND_OTP_LOADING:
      return {
        ...state,
        loading: true,
        error: false,
      };
    case SEND_OTP_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
        data: action.payload,
      };
    case SEND_OTP_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        errorData: action.payload,
      };
    case SEND_OTP_RESET:
      return initialState;

    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

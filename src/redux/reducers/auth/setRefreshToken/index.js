import reducerRegistry from '@redux/core/reducerRegistry';
import createActionName from '@redux/core/createActionName';

const initialState = {
  data: '',
};

const parent = 'AUTH';
export const reducerName = `${parent}-setRefreshToken`;

// SELECTORS //
export const selectData = (state) =>
  state[reducerName] ? state[reducerName].data : initialState.data;

// ACTIONS //
export const SET_AUTH_TOKEN = createActionName(reducerName, 'SUCCESS');

// ACTION CREATORS //
export const setRefreshToken = (payload) => {
  return {
    type: SET_AUTH_TOKEN,
    payload,
  };
};

// REDUCER //
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SET_AUTH_TOKEN:
      return {
        ...state,
        data: action.payload,
      };
    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

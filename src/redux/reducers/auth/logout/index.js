import { apiDispatcher } from '@api';

import reducerRegistry from '@redux/core/reducerRegistry';
import createActionName from '@redux/core/createActionName';

const initialState = {
  loading: false,
  error: false,
  data: '',
};

const parent = 'AUTH';
export const reducerName = `${parent}-logout`;

// SELECTORS //
export const selectData = (state) =>
  state[reducerName] ? state[reducerName].data : initialState.data;
export const selectErrorStatus = (state) =>
  state[reducerName] ? state[reducerName].error : initialState.error;
export const selectErrorData = (state) =>
  state[reducerName] ? state[reducerName].errorData : initialState.errorData;
export const selectLoadingStatus = (state) =>
  state[reducerName] ? state[reducerName].loading : initialState.loading;

// ACTIONS //
export const LOGOUT_BEGIN = createActionName(reducerName, 'BEGIN');
export const LOGOUT_SUCCESS = createActionName(reducerName, 'SUCCESS');
export const LOGOUT_ERROR = createActionName(reducerName, 'ERROR');

// ACTION CREATORS //
export const logoutBegin = () => {
  return {
    type: LOGOUT_BEGIN,
  };
};

export const logoutSuccess = (payload) => {
  return {
    type: LOGOUT_SUCCESS,
    payload,
  };
};

export const logoutError = (error) => {
  return {
    type: LOGOUT_ERROR,
    error,
  };
};

// ASYNC ACTIONS //
export const Logout = (payload) => {
  return (dispatch) => {
    return apiDispatcher('logout', payload);
  };
};

// REDUCER //
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOGOUT_BEGIN:
      return {
        ...state,
        loading: true,
      };
    case LOGOUT_SUCCESS:
      return {
        ...state,
        error: false,
        loading: false,
        data: action.payload,
      };
    case LOGOUT_ERROR:
      return {
        ...state,
        error: false,
        loading: false,
        errorData: action.error,
      };
    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

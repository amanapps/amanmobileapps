import { apiDispatcher } from '@api';

import reducerRegistry from '@redux/core/reducerRegistry';
import createActionName from '@redux/core/createActionName';
import { setUserID } from '../setUserID';
import { setRefreshToken } from '../setRefreshToken';
import { setAuthToken } from '../setAuthToken';

const initialState = {
  loading: false,
  error: false,
  errorData: null,
  data: null,
};

const parent = 'AUTH';
export const reducerName = `${parent}-refreshAuthToken`;

// SELECTORS //
export const selectData = (state) =>
  state[reducerName] ? state[reducerName].data : initialState.data;
export const selectErrorStatus = (state) =>
  state[reducerName] ? state[reducerName].error : initialState.error;
export const selectErrorData = (state) =>
  state[reducerName] ? state[reducerName].errorData : initialState.errorData;
export const selectLoadingStatus = (state) =>
  state[reducerName] ? state[reducerName].loading : initialState.loading;

// ACTIONS //
export const REFRESH_AUTH_TOKEN_ERROR = createActionName(reducerName, 'ERROR');
export const REFRESH_AUTH_TOKEN_LOADING = createActionName(
  reducerName,
  'LOADING'
);
export const REFRESH_AUTH_TOKEN_SUCCESS = createActionName(
  reducerName,
  'SUCCESS'
);
export const REFRESH_AUTH_TOKEN_RESET = createActionName(reducerName, 'RESET');

// ACTION CREATOR //
export const refreshAuthTokenLoading = () => ({
  type: REFRESH_AUTH_TOKEN_LOADING,
});

export const refreshAuthTokenError = (payload) => ({
  type: REFRESH_AUTH_TOKEN_ERROR,
  payload,
});

export const refreshAuthTokenSuccess = (payload) => ({
  type: REFRESH_AUTH_TOKEN_SUCCESS,
  payload,
});

export const refreshAuthTokenReset = () => ({ type: REFRESH_AUTH_TOKEN_RESET });

export const refreshAuthTokenApi = (refresh) => {
  return apiDispatcher('refresh-auth-token', { refresh });
};

export const refreshAuthToken = (refreshToken) => {
  return (dispatch) => {
    dispatch(refreshAuthTokenLoading());
    refreshAuthTokenApi(refreshToken).then((response) => {
      const { status, data } = response;
      if (status > 0 && status < 400) {
        // set current data
        dispatch(refreshAuthTokenSuccess(data.data));
      } else {
        dispatch(refreshAuthTokenError(data));
      }
    });
  };
};

// REDUCER //
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case REFRESH_AUTH_TOKEN_LOADING:
      return {
        ...state,
        loading: true,
        error: false,
      };
    case REFRESH_AUTH_TOKEN_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
        data: action.payload,
      };
    case REFRESH_AUTH_TOKEN_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        errorData: action.payload,
      };
    case REFRESH_AUTH_TOKEN_RESET:
      return initialState;

    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

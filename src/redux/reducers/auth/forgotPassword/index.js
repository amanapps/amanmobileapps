import { apiDispatcher } from '@api';

import reducerRegistry from '@redux/core/reducerRegistry';
import createActionName from '@redux/core/createActionName';

const initialState = {
  loading: false,
  error: false,
  errorData: null,
  data: null,
};

const parent = 'AUTH';
export const reducerName = `${parent}-forgotPassword`;

// SELECTORS //
export const selectData = state =>
  state[reducerName] ? state[reducerName].data : initialState.data;
export const selectErrorStatus = state =>
  state[reducerName] ? state[reducerName].error : initialState.error;
export const selectErrorData = state =>
  state[reducerName] ? state[reducerName].errorData : initialState.errorData;
export const selectLoadingStatus = state =>
  state[reducerName] ? state[reducerName].loading : initialState.loading;

// ACTIONS //
export const FORGOT_PASSWORD_ERROR = createActionName(reducerName, 'ERROR');
export const FORGOT_PASSWORD_LOADING = createActionName(reducerName, 'LOADING');
export const FORGOT_PASSWORD_SUCCESS = createActionName(reducerName, 'SUCCESS');
export const FORGOT_PASSWORD_RESET = createActionName(reducerName, 'RESET');

// ACTION CREATOR //
export const forgotPasswordLoading = () => ({ type: FORGOT_PASSWORD_LOADING });

export const forgotPasswordError = payload => ({
  type: FORGOT_PASSWORD_ERROR,
  payload,
});

export const forgotPasswordSuccess = payload => ({
  type: FORGOT_PASSWORD_SUCCESS,
  payload,
});

export const forgotPasswordReset = () => ({ type: FORGOT_PASSWORD_RESET });

export const forgotPassword = (login, password, password2) => {
  return dispatch => {
    dispatch(forgotPasswordLoading());
    apiDispatcher('forgot', { password, login, password2 }).then(response => {
      const { status, data } = response;
      if (status > 0 && status < 400) {
        // set current data
        dispatch(forgotPasswordSuccess(data));
      } else {
        dispatch(forgotPasswordError(data));
      }
    });
  };
};

// REDUCER //
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case FORGOT_PASSWORD_LOADING:
      return {
        ...state,
        loading: true,
        error: false,
      };
    case FORGOT_PASSWORD_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
        data: action.payload,
      };
    case FORGOT_PASSWORD_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        errorData: action.payload,
      };
    case FORGOT_PASSWORD_RESET:
      return initialState;

    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

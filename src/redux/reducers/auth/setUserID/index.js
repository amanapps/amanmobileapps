import reducerRegistry from '@redux/core/reducerRegistry';
import createActionName from '@redux/core/createActionName';

const initialState = {
  data: 0,
};

const parent = 'AUTH';
export const reducerName = `${parent}-setUserID`;

// SELECTORS //
export const selectData = state =>
  state[reducerName] ? state[reducerName].data : initialState.data;

// ACTIONS //
export const SET_USER_ID = createActionName(reducerName, 'SUCCESS');

// ACTION CREATORS //
export const setUserID = payload => {
  return {
    type: SET_USER_ID,
    payload,
  };
};

export const setUserIDAsync = id => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      dispatch(setUserID(id));
      resolve(id);
    });
  };
};

// REDUCER //
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SET_USER_ID:
      return {
        ...state,
        data: action.payload,
      };
    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

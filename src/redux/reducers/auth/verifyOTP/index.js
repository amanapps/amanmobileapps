import { apiDispatcher } from '@api';

import reducerRegistry from '@redux/core/reducerRegistry';
import createActionName from '@redux/core/createActionName';

const initialState = {
  loading: false,
  error: false,
  errorData: null,
  data: null,
};

const parent = 'AUTH';
export const reducerName = `${parent}-OTP-verify`;

// SELECTORS //
export const selectData = state =>
  state[reducerName] ? state[reducerName].data : initialState.data;
export const selectErrorStatus = state =>
  state[reducerName] ? state[reducerName].error : initialState.error;
export const selectErrorData = state =>
  state[reducerName] ? state[reducerName].errorData : initialState.errorData;
export const selectLoadingStatus = state =>
  state[reducerName] ? state[reducerName].loading : initialState.loading;

// ACTIONS //
export const OTP_ERROR = createActionName(reducerName, 'ERROR');
export const OTP_LOADING = createActionName(reducerName, 'LOADING');
export const OTP_SUCCESS = createActionName(reducerName, 'SUCCESS');
export const OTP_RESET = createActionName(reducerName, 'RESET');

// ACTION CREATOR //
export const otpLoading = () => ({ type: OTP_LOADING });

export const otpError = payload => ({ type: OTP_ERROR, payload });

export const otpSuccess = payload => ({ type: OTP_SUCCESS, payload });

export const otpReset = () => ({ type: OTP_RESET });

export const verifyOTP = (Mobile, OTP) => {
  return dispatch => {
    dispatch(otpLoading());
    apiDispatcher('verify_otp', { OTP, Mobile }).then(response => {
      const { status, data } = response;
      if (status > 0 && status < 400) {
        dispatch(otpSuccess(data));
      } else {
        dispatch(otpError(data));
      }
    });
  };
};

// REDUCER //
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case OTP_LOADING:
      return {
        ...state,
        loading: true,
        error: false,
      };
    case OTP_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
        data: action.payload,
      };
    case OTP_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        errorData: action.payload,
      };
    case OTP_RESET:
      return initialState;

    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

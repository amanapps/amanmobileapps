import { apiDispatcher } from '@api';

import reducerRegistry from '@redux/core/reducerRegistry';
import createActionName from '@redux/core/createActionName';

const initialState = {
  loading: false,
  error: false,
  errorData: null,
  data: null,
  postData: {
    mobile: '',
    name: '',
    password: '',
    day: '',
    month: '',
    year: '',
    street: '',
    street2: '',
    city: '',
    state_id: '',
    zip: '',
    country_id: 'IQ',
    union_id: '',
    carrier_id: '',
    profession_id: '',
    mother_name: '',
    gender: '',
  },
  // postData: {
  //   mobile: '9938533890',
  //   name: 'Bansl',
  //   password: 'q2w3e4r5t',
  //   day: '',
  //   month: '',
  //   year: '',
  //   street: 'qwert',
  //   street2: 'sdgsfhs',
  //   city: '',
  //   state_id: '',
  //   zip: '',
  //   country_id: 'IQ',
  //   union_id: 133,
  //   carrier_id: '',
  //   profession_id: '',
  //   mother_name: '',
  //   gender: 'male',
  //   business_type_id: 2,
  //   location: 'gsgsf',
  //   state_code: 1408,
  // },
};

const parent = 'AUTH';
export const reducerName = `${parent}-registration`;

// SELECTORS //
export const selectData = state =>
  state[reducerName] ? state[reducerName].data : initialState.data;
export const selectErrorStatus = state =>
  state[reducerName] ? state[reducerName].error : initialState.error;
export const selectErrorData = state =>
  state[reducerName] ? state[reducerName].errorData : initialState.errorData;
export const selectLoadingStatus = state =>
  state[reducerName] ? state[reducerName].loading : initialState.loading;
export const selectPostData = state =>
  state[reducerName] ? state[reducerName].postData : initialState.postData;

// ACTIONS //
export const REGISTRATION_ERROR = createActionName(reducerName, 'ERROR');
export const REGISTRATION_LOADING = createActionName(reducerName, 'LOADING');
export const REGISTRATION_SUCCESS = createActionName(reducerName, 'SUCCESS');
export const REGISTRATION_RESET = createActionName(reducerName, 'RESET');
export const REGISTRATION_POST_DATA = createActionName(
  reducerName,
  'REGISTRATION_POST_DATA',
);

// ACTION CREATOR //
export const otpLoading = () => ({ type: REGISTRATION_LOADING });

export const otpError = payload => ({ type: REGISTRATION_ERROR, payload });

export const otpSuccess = payload => ({ type: REGISTRATION_SUCCESS, payload });

export const postData = payload => ({ type: REGISTRATION_POST_DATA, payload });

export const reset = () => ({ type: REGISTRATION_RESET });

export const setPostData = (type, data) => {
  return (dispatch, getState) => {
    const d = selectPostData(getState());
    dispatch(
      postData({
        ...d,
        [type]: data,
      }),
    );
  };
};

export const registerUser = () => {
  return (dispatch, getState) => {
    dispatch(otpLoading());
    apiDispatcher('registration', selectPostData(getState())).then(response => {
      const { status, data } = response;
      if (status > 0 && status < 400) {
        dispatch(otpSuccess(data));
      } else {
        dispatch(otpError(data));
      }
    });
  };
};

export const merchantRegisterUser = () => {
  return (dispatch, getState) => {
    dispatch(otpLoading());
    const d = selectPostData(getState());
    console.log('REf', d);
    apiDispatcher('merchant_registration', d).then(response => {
      const { status, data } = response;
      if (status > 0 && status < 400) {
        dispatch(otpSuccess(data));
      } else {
        dispatch(otpError(data));
      }
    });
  };
};

// REDUCER //
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case REGISTRATION_LOADING:
      return {
        ...state,
        loading: true,
        error: false,
      };
    case REGISTRATION_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
        data: action.payload,
      };
    case REGISTRATION_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        errorData: action.payload,
      };
    case REGISTRATION_POST_DATA:
      return {
        ...state,
        postData: action.payload,
      };
    case REGISTRATION_RESET:
      return initialState;

    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

import reducerRegistry from '@redux/core/reducerRegistry';
import createActionName from '@redux/core/createActionName';

const initialState = {
  type: null,
  props: {},
};

const reducerName = 'toast';

// SELECTORS //
export const selectToastType = (state) => state[reducerName].type;
export const selectToastProps = (state) => state[reducerName].props;

// ACTIONS //
export const SHOW_TOAST = createActionName(reducerName, 'SHOW_TOAST');
export const HIDE_TOAST = createActionName(reducerName, 'HIDE_TOAST');

// ACTION CREATORS //
export const showToast = (type, props) => ({
  type: SHOW_TOAST,
  payload: {
    type,
    props,
  },
});

export const hideToast = (type, props) => ({
  type: HIDE_TOAST,
  payload: {
    type,
    props,
  },
});

// REDUCER //
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SHOW_TOAST:
      return {
        ...state,
        type: action.payload.type,
        props: { ...action.payload.props, show: true },
      };
    case HIDE_TOAST:
      return {
        ...state,
        type: action.payload.type,
        props: { ...action.payload.props, show: false },
      };
    default:
      return state;
  }
}

reducerRegistry.register(reducerName, reducer);

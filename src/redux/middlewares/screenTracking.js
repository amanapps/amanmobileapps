// @flow
import { NavigationActions } from 'react-navigation';

// gets the current screen from navigation state
function getActiveRouteName(navigationState) {
  if (!navigationState) {
    return null;
  }
  const route = navigationState.routes[navigationState.index];
  // dive into nested navigators
  if (route.routes) {
    return getActiveRouteName(route);
  }
  return route.routeName;
}

const screenTracking =
  ({ getState }) =>
  next =>
  action => {
    if (
      action.type !== NavigationActions.NAVIGATE &&
      action.type !== NavigationActions.BACK
    ) {
      return next(action);
    }

    const currentScreen = getActiveRouteName(getState().navigation);
    const result = next(action);
    const nextScreen = getActiveRouteName(getState().navigation);
    if (nextScreen !== currentScreen) {
      // the line below uses the Google Analytics tracker
      // change the tracker here to use other Mobile analytics SDK.
      // console.log(nextScreen);
      // FA.setCurrentScreen(nextScreen);
    }
    return result;
  };

export default screenTracking;

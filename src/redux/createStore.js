import thunk from 'redux-thunk';
import promiseMiddleware from 'redux-promise';
import { compose, createStore, applyMiddleware, combineReducers } from 'redux';

import reducerRegistry from '@redux/core/reducerRegistry';

import rootReducer from './rootReducer';

// Preserve initial state for not-yet-loaded reducers
const combine = (reducers, initialState) => {
  const reducerNames = Object.keys(reducers);
  if (typeof initialState !== 'undefined') {
    // console.log(initialState);
    // console.table(reducerNames);
    // console.table(Object.keys(initialState));
    Object.keys(initialState).forEach((item) => {
      if (reducerNames.indexOf === -1) {
        reducers[item] = (state = null) => state;
      }
    });
  }
  return combineReducers(reducers);
};

// creates the store
const configureStore = (initialState) => {
  /* ------------- Redux Configuration ------------- */

  const middlewares = [];

  middlewares.push(promiseMiddleware);
  middlewares.push(thunk);
  // middlewares.push(screenTracking);
  // middlewares.push(reduxMiddleware);

  // eslint-disable-next-line no-undef
  if (__DEV__) {
    // const logger = createLogger();
    // middlewares.push(logger);
  }

  /* ------------- Assemble Middleware ------------- */
  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const enhancer = composeEnhancers(applyMiddleware(...middlewares))(
    createStore
  );

  const reducer =
    typeof initialState !== 'undefined'
      ? combine(reducerRegistry.getReducers(), initialState)
      : rootReducer;

  const store = enhancer(reducer, initialState);

  // Replace the store's reducer whenever a new reducer is registered.
  reducerRegistry.setChangeListener((reducers) => {
    const reducer = combine(reducers);
    store.replaceReducer(reducer);
  });

  /* ------------- Hot reload reducers ------------- */
  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('./rootReducer', () => {
      const nextRootReducer = require('./rootReducer');
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
};

export default configureStore;

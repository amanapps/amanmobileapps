// eslint-disable-next-line no-extend-native
Array.prototype.flat = function () {
  return [].concat(...this);
};

export class ReducerRegistry {
  constructor() {
    this.reducers = {};
    this.emitChange = null;
  }

  getReducers() {
    return { ...this.reducers };
  }

  register(name, reducer) {
    this.reducers = { ...this.getReducers(), [name]: reducer };
    if (this.emitChange) {
      this.emitChange(this.getReducers());
    }
  }

  setChangeListener(listener) {
    this.emitChange = listener;
  }
}

const reducerRegistry = new ReducerRegistry();
export default reducerRegistry;

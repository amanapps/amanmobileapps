import { combineReducers } from 'redux';

import toastReducer from '@redux/reducers/toastReducer';
import bottomSheetReducer from '@redux/reducers/bottomSheetReducer';
import rootLoaderReducer from '@redux/reducers/rootLoaderReducer';

// const formations = constants.formations;

const appReducer = combineReducers({
  toast: toastReducer,
  bottomSheet: bottomSheetReducer,
  loader: rootLoaderReducer,
});

export default appReducer;

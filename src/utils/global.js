const MAX_TEAM_CREATION_COUNT = 11;

export function getCookie(cname) {
  var name = cname + '=';
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length);
    }
  }
  return '';
}

export function deleteCookie(name) {
  document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

export function getQueryParams(qs) {
  qs = qs.split('+').join(' ');

  var params = {},
    tokens,
    re = /[?&]?([^=]+)=([^&]*)/g;

  while ((tokens = re.exec(qs))) {
    params[decodeURIComponent(tokens[1])] = decodeURIComponent(tokens[2]);
  }

  return params;
}

export function convertMillion(number) {
  return parseFloat(number / 1000000.0);
}

export function removeSpace(string) {
  return string.replace(/ /g, '-');
}

export function removeDash(string) {
  return string.replace(/-/g, ' ');
}

export function convertToUrlCase(string) {
  const str = removeDash(string.toLowerCase());
  return removeSpace(str);
}

// Function to add suffixes to numbers
export function addSuffixToNum(num) {
  let j = num % 10,
    k = num % 100;

  if (j === 1 && k !== 11) {
    return num + 'st';
  }

  if (j === 2 && k !== 12) {
    return num + 'nd';
  }

  if (j === 3 && k !== 13) {
    return num + 'rd';
  }

  return num + 'th';
}

export function checkDeadline(league) {
  const match_status = league.get_match.status;
  return match_status !== 0; // 0 is for match not started yet
}

// Function to calculate age from DOB

export function calculateAge(dateString) {
  const today = new Date();
  const birthDate = new Date(dateString);
  let age = today.getFullYear() - birthDate.getFullYear();
  const m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age--;
  }
  return age;
}

export function getTimeRemaining(endtime) {
  const t = Date.parse(endtime) - Date.parse(new Date());
  const seconds = Math.floor((t / 1000) % 60);
  const minutes = Math.floor((t / 1000 / 60) % 60);
  const hours = Math.floor((t / (1000 * 60 * 60)) % 24);
  const days = Math.floor(t / (1000 * 60 * 60 * 24));
  return {
    total: t,
    days: days,
    hours: hours,
    minutes: minutes,
    seconds: seconds,
  };
}

export function initializeClock(id, endtime) {
  const clock = document.getElementById(id);
  const daysSpan = clock.querySelector('.days');
  const hoursSpan = clock.querySelector('.hours');
  const minutesSpan = clock.querySelector('.minutes');
  const secondsSpan = clock.querySelector('.seconds');

  const timeObj = {};

  function updateClock() {
    const t = getTimeRemaining(endtime);

    const days = t.days;
    const hours = `0${t.hours}`.slice(-2);
    const minutes = `0${t.minutes}`.slice(-2);
    const seconds = `0${t.seconds}`.slice(-2);

    if (days > 0) {
      daysSpan.innerHTML = days;
    }
    hoursSpan.innerHTML = hours;
    minutesSpan.innerHTML = minutes;
    if (days < 1) {
      secondsSpan.innerHTML = seconds;
    }

    if (t.total <= 0) {
      clearInterval(timeinterval);
    }

    timeObj.days = days;
    timeObj.hours = hours;
    timeObj.minutes = minutes;
    timeObj.seconds = seconds;

    //console.log(timeObj);
  }

  updateClock();
  var timeinterval = setInterval(updateClock, 1000);

  return timeObj;
}

// Retunr key name assosicate with value
export const getKeyByValue = (object, value) => {
  return Object.keys(object).find((key) => object[key] === value);
};

// convert array to an object
export const convertArrayToObject = (array, key) => {
  const initialValue = {};
  return array.reduce((obj, item) => {
    return {
      ...obj,
      [item[key]]: item,
    };
  }, initialValue);
};

export const checkMaxTeamCreated = (league) => {
  return !league
    ? false
    : league.total_created_teams >= MAX_TEAM_CREATION_COUNT;
};

/**
 * Function for string template literal from key value pair
 * e.g: templateFormat('This is {name}', {'name': 'your name'});
 */
export const stringFormat = (text, values) => {
  let formatted = text;
  if (typeof values === 'object') {
    Object.entries(values).map(([key, value]) => {
      const regexp = new RegExp(`\\{${key}\\}`, 'gi');
      formatted = formatted.replace(regexp, value);
      return formatted;
    });
  }
  return formatted;
};

export const formatInt = (num) => {
  return `${num}`.padStart(2, '0');
};

function dateformat(obj, format) {
  const monthNames = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];
  const year = obj.getFullYear();
  const month = obj.getMonth();
  const date = obj.getDate();

  let dFormat = format;
  if (!dFormat) {
    dFormat = 'MM/dd/yyyy';
  }

  if (dFormat.indexOf('yyyy') > -1) {
    dFormat = dFormat.replace('yyyy', year);
  }

  if (dFormat.indexOf('dd') > -1) {
    dFormat = dFormat.replace('dd', formatInt(date));
  }

  if (dFormat.indexOf('MMM') > -1) {
    dFormat = dFormat.replace('MMM', monthNames[month].substr(0, 3));
  } else if (dFormat.indexOf('MM') > -1) {
    dFormat = dFormat.replace('MM', formatInt(month + 1));
  }

  if (dFormat.indexOf('HH') > -1) {
    dFormat = dFormat.replace('HH', formatInt(obj.getHours()));
  }

  if (dFormat.indexOf('hh') > -1) {
    let hours = obj.getHours();
    if (hours > 12) {
      hours -= 12;
    }
    dFormat = dFormat.replace('hh', formatInt(hours));
  }

  if (dFormat.indexOf('mm') > -1) {
    dFormat = dFormat.replace('mm', formatInt(obj.getMinutes()));
  }
  if (dFormat.indexOf('ss') > -1) {
    dFormat = dFormat.replace('ss', formatInt(obj.getSeconds()));
  }
  if (dFormat.indexOf('z') > -1) {
    let time = 'AM';
    if (obj.getHours() > 12) {
      time = 'PM';
    }
    dFormat = dFormat.replace('z', time);
  }
  return dFormat;
}
export { dateformat };

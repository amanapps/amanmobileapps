import { Dimensions, Platform } from 'react-native';

const d = Dimensions.get('window');

export const isIOS = Platform.OS === 'ios';

export const isAndroid = Platform.OS === 'android';

export const isiPhoneX = !!(
  Platform.OS === 'ios' &&
  (d.height > 800 || d.width > 800)
);

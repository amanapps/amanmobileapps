export const MEETING_STATUS_TYPE = {
  ALL: -1,
  DRAFT: 0,
  CANCELLED: 1,
  ACTIVE: 2,
};

export const MEETING_STATUS_REVERSE_TYPE = {
  0: 'Draft',
  1: 'Cancelled',
  2: 'Active',
};

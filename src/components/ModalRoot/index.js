/* @flow */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import { selectModalProps, selectModalType } from '@redux/reducers/modals';
import InformationModal from '../Modals/InformationModal';

const MODAL_COMPONENTS = {
  INFORMATION_MODAL: InformationModal,
};

const ModalRoot = ({ type, props }) => {
  if (!type) {
    return null;
  }

  const ModalComponent = MODAL_COMPONENTS[type];
  return <ModalComponent {...props} />;
};

ModalRoot.propTypes = {
  props: PropTypes.object.isRequired,
  type: PropTypes.string,
};

const mapStateToProps = createStructuredSelector({
  type: selectModalType,
  props: selectModalProps,
});

export default connect(mapStateToProps)(ModalRoot);

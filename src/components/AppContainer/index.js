import React from 'react';
import { StyleSheet } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';

const AppContainer = ({ children, ...props }) => {
  return (
    <SafeAreaView
      {...props}
      style={[styles.safearea, props.style]}
      edges={['bottom']}>
      {children}
    </SafeAreaView>
  );
};

export default AppContainer;

const styles = StyleSheet.create({
  safearea: {
    flex: 1,
    paddingTop: 16,
    // backgroundColor: '#fff',
  },
});

/* @flow */
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import Modal from 'react-native-modal';
import {
  StyleSheet,
  View,
  StatusBar,
  KeyboardAvoidingView,
} from 'react-native';
import { colors } from '@config/theme';
import { isAndroid } from '@utils/platformUtils';

const styles = StyleSheet.create({
  container: {
    marginVertical: 0,
    marginHorizontal: 10,
  },
});

const ModalContainer = props => {
  useEffect(() => {
    if (isAndroid) {
      const color = props.isVisible ? 'rgba(0,0,0,1)' : colors.primary;
      StatusBar.setBackgroundColor(color, true);
    }
  }, [props.isVisible]);

  const { style, children, transparent } = props;
  const backdropOpacity = transparent ? 0 : props.backdropOpacity;

  return (
    <View>
      <Modal
        useNativeDriver
        hardwareAccelerated
        hideModalContentWhileAnimating
        backdropOpacity={backdropOpacity}
        {...props}
        style={[styles.container, style]}>
        {isAndroid ? (
          children
        ) : (
          <KeyboardAvoidingView behavior="position">
            {children}
          </KeyboardAvoidingView>
        )}
      </Modal>
    </View>
  );
};

ModalContainer.propTypes = {
  children: PropTypes.element.isRequired,
  backdropOpacity: PropTypes.number,
};

ModalContainer.defaultProps = {
  backdropOpacity: 0.8,
};

export default ModalContainer;

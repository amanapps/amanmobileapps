/* @flow */
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Modal from 'react-native-modal';
import React, { useEffect } from 'react';
import { Text } from 'react-native-paper';

import { createStructuredSelector } from 'reselect';
import { View, StyleSheet, StatusBar } from 'react-native';

import { colors } from '@config/theme';
import globalStyles from '@config/globalStyles';
import { isAndroid } from '@utils/platformUtils';

import Loader from '@components/Loader';

import { selectType, selectProps } from '@redux/reducers/rootLoaderReducer';

const RootLoader = ({ type, props }) => {
  useEffect(() => {
    if (isAndroid) {
      const color = props.show ? 'rgba(0,0,0,1)' : colors.colorPrimary;
      StatusBar.setBackgroundColor(color, true);
    }
  }, [props.show]);

  if (props.show) {
    return (
      <Modal
        isVisible
        useNativeDriver
        backdropOpacity={0.7}
        animationIn="zoomIn"
        animationOut="zoomOut"
        style={styles.container}
        hideModalContentWhileAnimating>
        <View style={styles.wrapper}>
          <View style={styles.loaderContainer}>
            <Loader />
          </View>
          <View style={styles.loaderTextContainer}>
            <Text weight="medium" style={styles.loadingText}>
              Loading...
            </Text>
          </View>
        </View>
      </Modal>
    );
  }

  return null;
};

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 16,
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  wrapper: {
    borderRadius: 2,
    paddingHorizontal: 15,
    flexDirection: 'row',
    backgroundColor: '#fff',
    alignContent: 'center',
  },
  loaderContainer: {
    width: 60,
    height: 80,
    ...globalStyles.center,
  },
  loaderTextContainer: {
    paddingLeft: 5,
    justifyContent: 'center',
  },
  loadingText: {
    fontSize: 16,
    color: colors.black,
  },
});

RootLoader.propTypes = {
  props: PropTypes.object.isRequired,
  type: PropTypes.string,
};

const mapStateToProps = createStructuredSelector({
  type: selectType,
  props: selectProps,
});

export default connect(mapStateToProps)(RootLoader);

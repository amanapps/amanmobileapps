import React, { useCallback, useState } from 'react';
import { connect } from 'react-redux';
import { Avatar, Button, Text, TouchableRipple } from 'react-native-paper';
import { createStructuredSelector } from 'reselect';
import { View, StyleSheet, Image, ImageBackground } from 'react-native';

import { i18n } from '@i18n';
import { selectData as userProfile } from '../../redux/reducers/user/getDetails';

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
  },
  imageContainer: {
    marginVertical: 8,
    borderRadius: 11,
    overflow: 'hidden',
    alignSelf: 'center',
  },
  imageBackground: {
    padding: 16,
  },
  nameContainer: {
    marginStart: 16,
    justifyContent: 'center',
  },
  bold: {
    fontFamily: 'Poppins-Bold',
  },
  name: {
    color: '#171717',
  },
});

const Author = props => {
  const { profile, width, showAction, onSignOut, onEdit, onContactUs } = props;
  const [uri, SetURI] = useState(profile.image_url);

  const onError = useCallback(() => SetURI('user'), []);
  return (
    <View style={[styles.imageContainer, { width }]}>
      <ImageBackground
        source={{ uri: 'profile_background' }}
        resizeMethod="resize"
        resizeMode="cover"
        style={[styles.row, styles.imageBackground]}>
        <Avatar.Image
          size={65}
          source={({ size }) => {
            return (
              <Image
                source={{ uri }}
                onError={onError}
                style={{ width: size, height: size, borderRadius: size / 2 }}
              />
            );
          }}
        />
        {showAction && (
          <Button
            onPress={onEdit}
            style={{ position: 'absolute', right: 0 }}
            labelStyle={{ color: '#FC6011', letterSpacing: 0, fontSize: 12 }}
            icon={({ size }) => (
              <Image
                source={{ uri: 'edit' }}
                style={{ width: size, height: size }}
                resizeMode="center"
              />
            )}>
            Edit Profile
          </Button>
        )}
        <View style={styles.nameContainer}>
          <Text style={styles.name}>{i18n.t('home_hi')}</Text>
          <Text style={[styles.bold, styles.name]}>{profile.name}</Text>
          {showAction && (
            <View style={styles.row}>
              <TouchableRipple
                onPress={onSignOut}
                style={{
                  backgroundColor: '#000',
                  paddingHorizontal: 12,
                  borderRadius: 12,
                  paddingTop: 2,
                  alignSelf: 'flex-start',
                }}>
                <Text style={{ color: '#fff' }}>{i18n.t('sign_out')}</Text>
              </TouchableRipple>

              <TouchableRipple
                onPress={onContactUs}
                style={{
                  backgroundColor: '#fff',
                  paddingHorizontal: 12,
                  borderRadius: 12,
                  paddingTop: 2,
                  alignSelf: 'flex-start',
                  marginStart: 16,
                }}>
                <Text style={{ color: '#000' }}>{i18n.t('contact_us')}</Text>
              </TouchableRipple>
            </View>
          )}
        </View>
      </ImageBackground>
    </View>
  );
};

const mapStateToProps = createStructuredSelector({
  profile: userProfile,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Author);

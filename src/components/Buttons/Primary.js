import React from 'react';
import { View, Platform, StyleSheet, TouchableHighlight } from 'react-native';

import { colors } from '../../config/theme';
import Poppins from '../Typography/Poppins';

const Primary = ({ style, title, disabled, ...rest }) => {
  return (
    <View style={styles.wrapper}>
      <TouchableHighlight
        {...rest}
        onPress={disabled ? null : rest.onPress}
        style={[styles.button, style, disabled && styles.buttonDisabled]}
      >
        <Poppins
          weight="semiBold"
          style={[styles.text, disabled && styles.textDisabled]}
        >
          {title}
        </Poppins>
      </TouchableHighlight>
    </View>
  );
};

export default Primary;

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
  },
  button: {
    flex: 1,
    margin: 4,
    elevation: 4,
    backgroundColor: colors.colorPrimary,
    borderRadius: 8,
  },
  text: {
    textAlign: 'center',
    margin: 8,
    color: colors.colorOnPrimary,
    ...Platform.select({
      ios: {
        fontSize: 18,
      },
      android: {
        paddingVertical: 4,
        fontWeight: '500',
      },
    }),
  },
  buttonDisabled: Platform.select({
    ios: {
      backgroundColor: '#a1a1a1',
    },
    android: {
      elevation: 0,
      backgroundColor: '#dfdfdf',
    },
  }),
  textDisabled: Platform.select({
    ios: {
      color: '#cdcdcd',
    },
    android: {
      color: '#a1a1a1',
    },
  }),
});

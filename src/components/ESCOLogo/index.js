import React from 'react';
import { Image, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  logo: {
    width: 134,
    height: 65,
  },
});

const ESCOLogo = ({ style }) => {
  return (
    <Image
      resizeMode="contain"
      style={[styles.logo, style]}
      source={require('../../assets/images/esco_logo.png')}
    />
  );
};

export default ESCOLogo;

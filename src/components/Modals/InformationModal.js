// @flow
import { connect } from 'react-redux';
import React, { useCallback } from 'react';
import { StyleSheet, View } from 'react-native';
import { createStructuredSelector } from 'reselect';
import {
  Text,
  Subheading,
  Paragraph,
  TouchableRipple,
  Title,
} from 'react-native-paper';
import { SafeAreaView } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/Octicons';

import { i18n } from '@i18n';
import { colors } from '@config/theme';
import ModalContainer from '@components/ModalContainer';

import { hideModal } from '@redux/reducers/modals';

const InformationModal = props => {
  const { mode, show, hide, message, onPress } = props;

  const closeModal = useCallback(() => {
    hide('INFORMATION_MODAL', props);
    if (typeof onPress === 'function') {
      onPress();
    }
  }, [hide, onPress, props]);

  let color, buttonText, icon, title, titleColor;
  if (mode === 'error') {
    color = colors.red300;
    titleColor = colors.red900;
    icon = 'alert';
    title = i18n.t('oh_snap');
    buttonText = i18n.t('dismiss');
  } else if (mode === 'info') {
    color = colors.blue300;
    titleColor = colors.blue900;
    icon = 'info';
    title = i18n.t('oh_snap');
    buttonText = i18n.t('ok_label');
  } else {
    color = colors.green300;
    titleColor = colors.green900;
    icon = 'verified';
    title = i18n.t('that_great');
    buttonText = i18n.t('ok_label');
  }

  return (
    <ModalContainer
      isVisible={show}
      style={styles.container}
      onBackdropPress={closeModal}
      onBackButtonPress={closeModal}>
      <SafeAreaView style={styles.wrapper}>
        <View style={styles.messageContainer}>
          <Icon size={48} name={icon} color={color} style={styles.icon} />
          <Title style={[styles.title, { color: titleColor }]}>{title}</Title>
          <Paragraph style={styles.message}>{message}</Paragraph>
        </View>
        <TouchableRipple
          onPress={closeModal}
          style={[styles.button, { backgroundColor: color }]}>
          <Text style={styles.buttonText}>{buttonText}</Text>
        </TouchableRipple>
      </SafeAreaView>
    </ModalContainer>
  );
};

const mapStateToProps = createStructuredSelector({});

const mapDispatchToProps = {
  hide: hideModal,
};
export default connect(mapStateToProps, mapDispatchToProps)(InformationModal);

const styles = StyleSheet.create({
  container: {
    marginHorizontal: '5%',
    justifyContent: 'center',
  },
  wrapper: {
    borderRadius: 3,
    backgroundColor: colors.white,
  },
  messageContainer: {
    paddingVertical: 10,
    paddingHorizontal: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    marginVertical: 16,
  },
  title: {
    color: '#000',
    opacity: 0.8,
  },
  message: {
    color: '#000',
    opacity: 0.7,
    textTransform: 'capitalize',
    textAlign: 'center',
  },
  button: {
    backgroundColor: colors.red300,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomStartRadius: 3,
    borderBottomEndRadius: 3,
  },
  buttonText: {
    paddingVertical: 12,
    lineHeight: 20,
    color: '#fff',
  },
});

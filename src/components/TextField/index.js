import React from 'react';
import { withTheme } from 'react-native-paper';
import { StyleSheet, Image, View, TextInput, Platform } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const TextField = props => {
  const { icon, iconStyle, style, theme, ...rest } = props;

  return (
    <View style={styles.container}>
      {typeof icon === 'object' ? (
        <Image
          source={icon}
          resizeMode="center"
          style={[iconStyle, styles.iconStyle]}
        />
      ) : (
        <MaterialCommunityIcons
          size={24}
          name={icon}
          color={theme.colors.primary}
        />
      )}
      <TextInput {...rest} style={[styles.textfield, style]} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    borderRadius: 12,
    borderColor: '#1d1d1d',
    borderWidth: StyleSheet.hairlineWidth,
    paddingHorizontal: 8,
    ...Platform.select({
      ios: {
        paddingVertical: 8,
      },
      android: {
        alignItems: 'center',
      },
    }),
    marginBottom: 16,
  },
  iconStyle: {
    width: 36,
    height: 36,
  },
  textfield: {
    flex: 1,
    marginHorizontal: 4,
    fontFamily: 'Poppins-Medium',
  },
});

export default withTheme(TextField);

// @flow
import React from 'react';
import { View, StyleSheet, ActivityIndicator } from 'react-native';

import { colors } from '@config/theme';

const styles = StyleSheet.create({
  loaderContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const Loader = props => {
  const { size, color, style } = props;

  return (
    <View style={[styles.loaderContainer, style]}>
      <ActivityIndicator size={size} color={color} />
    </View>
  );
};

Loader.defaultProps = {
  size: 'large',
  color: colors.colorPrimary,
};

export default Loader;

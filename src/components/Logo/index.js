import React from 'react';
import { Image, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  logo: {
    width: 300,
    height: 100,
  },
});

const Logo = ({ style }) => {
  return (
    <Image
      source={{ uri: 'logo_main' }}
      style={[styles.logo, style]}
      resizeMode="contain"
    />
  );
};

export default Logo;

let host = 'http://172.104.133.224:8013';

if (process.env.REACT_APP_BUILD_HOST) {
  host = process.env.REACT_APP_BUILD_HOST;
}

export default host;

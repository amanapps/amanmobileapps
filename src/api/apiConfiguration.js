import host from './host';

export default {
  aman: {
    host,
    api: {
      login: {
        path: 'api/auth/token',
        method: 'post',
        authentication: false,
      },
      registration: {
        path: 'mobile/member/signup',
        method: 'post',
        authentication: false,
      },
      merchant_registration: {
        path: 'api/merchant/signup',
        method: 'post',
        authentication: false,
      },
      send_otp: {
        path: 'api/mobile/request/otp',
        method: 'post',
        authentication: true,
      },
      verify_otp: {
        path: 'api/mobile/verify/otp',
        method: 'post',
        authentication: true,
      },
      forgot: {
        path: 'api/resetPassword',
        method: 'post',
        authentication: false,
      },
      profile: {
        path: 'api/member/profile/view',
        method: 'get',
        authentication: true,
      },
      merchant_profile: {
        path: 'api/merchant/profile/view',
        method: 'post',
        authentication: true,
      },
      fetch_service_type: {
        path: 'member/request_types/all',
        method: 'post',
        authentication: true,
      },
      fetch_states: {
        path: 'member/states/country',
        method: 'post',
        authentication: true,
      },
      advertisement: {
        path: 'api/advertisement/types',
        method: 'post',
        authentication: true,
      },
      advertisement_list: {
        path: 'api/merchant/advertisement/list',
        method: 'post',
        authentication: true,
      },
      committee_member_search: {
        path: 'api/committee/member/search',
        method: 'post',
        authentication: true,
      },
      transaction_invoice: {
        path: 'api/merchant/transactions',
        method: 'post',
        authentication: true,
      },
      work_type_list: {
        path: 'api/work/types',
        method: 'post',
        authentication: true,
      },
      log_activity: {
        path: 'api/committee/ActivityLog',
        method: 'post',
        authentication: true,
      },
      business_type: {
        path: 'api/business/types',
        method: 'post',
        authentication: true,
      },
      related_union: {
        path: 'member/unions/all',
        method: 'post',
        authentication: true,
      },
      contact_us: {
        path: 'api/merchant/contactus',
        method: 'post',
        authentication: true,
      },
      update_profile: {
        path: 'api/merchant/profile/update',
        method: 'post',
        authentication: true,
      },
    },
  },
};

import { stringFormat } from '@utils/global';

import host from './host';
import { AuthApi, Api } from './AuthApi';
import ApiConfiguration from './apiConfiguration';
import CookieManager from '@react-native-cookies/cookies';

const HOST = {
  AMAN: 'aman',
};

function getHostObject(hostName = '') {
  return ApiConfiguration[hostName] || {};
}

function getConfiguration(requestName = '', hostName = '') {
  const { api } = getHostObject(hostName);
  return api[requestName] || undefined;
}

function getCompleteUrl(hostName, path, templateValue) {
  if (hostName === HOST.AMAN) {
    return stringFormat(`${host}/${path}`, templateValue);
  }

  const { host: APIHOST } = getHostObject(hostName);
  return stringFormat(`${APIHOST}/${path}`, templateValue);
}

/**
 *
 * @param {*} requestName
 * @param {*} requestParams
 * @param {*} queryParams
 * @param {*} templateValue
 */
export function apiDispatcher(
  requestName = '',
  requestParams = null,
  queryParams = null,
  templateValue = {},
) {
  return apiDispatcherImpl(
    requestName,
    requestParams,
    queryParams,
    templateValue,
    HOST.AMAN,
  );
}

function apiDispatcherImpl(
  requestName = '',
  requestParams = null,
  queryParams = null,
  templateValue = {},
  hostName = null,
) {
  const configObject = getConfiguration(requestName, hostName);

  if (!configObject) {
    console.log(
      'Request name not found in configuration file, ' +
        'please check your request name or add api details in configuration file.',
    );
    return;
  }

  const { path, method, authentication } = configObject;

  const Router = authentication ? AuthApi : Api;

  let helperParams = {};
  const fd = new FormData();

  if (requestParams) {
    Object.keys(requestParams).forEach(key =>
      fd.append(key, requestParams[key]),
    );
  }

  if (authentication) {
    fd.append('access_token', AuthApi.defaults.headers.Authorization);
  }

  if (queryParams) {
    helperParams = {
      params: queryParams,
    };
  }

  CookieManager.clearAll(true);
  return Api({
    method,
    url: getCompleteUrl(hostName, path, templateValue),
    data: fd,
    ...helperParams,
  })
    .then(response => {
      if (__DEV__) {
        console.log('##', response);
      }
      const {
        data: { status_code, ...restData },
      } = response;
      return {
        ...response,
        status: status_code,
        data: restData,
      };
    })
    .catch(error => {
      console.log(error.request, error.response);
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        return error.response;
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        return error.request;
      } else {
        // Something happened in setting up the request that triggered an Error
        return {
          status: 400,
          message: error.message,
        };
      }
    });
}

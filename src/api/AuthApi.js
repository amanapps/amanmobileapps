import { Platform } from 'react-native';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import axios from 'axios';

import { selectData, reset } from '@redux/reducers/auth/setAuthToken';

import host from './host';
import { useEffect } from 'react';

const headers = {
  'content-type': `multipart/form-data; boundary=******${new Date().getTime()}******`,
};

// Set config defaults when creating the instance
export const AuthApi = axios.create({
  baseURL: host,
  headers,
  withCredentials: true,
});

export const Api = axios.create({
  headers,
  withCredentials: true,
});

const mapStateToProps = createStructuredSelector({
  token: selectData,
});

const mapDispatchToProps = {
  Reset: reset,
};

export const UpdateTokenInHeader = connect(
  mapStateToProps,
  null,
)(({ token, Reset }) => {
  console.log('setting up token', token);
  AuthApi.defaults.headers.Authorization = token;
  AuthApi.defaults.headers.platfrom = Platform.OS;

  useEffect(() => Reset);

  return null;
});

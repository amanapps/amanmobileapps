import { StyleSheet, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

export const VIEWPORT_WIDTH = width;
export const VIEWPORT_HEIGHT = height;

// * Calculating main content height * //
// * Viewport height - Status Bar - App Top Bar - * //
export const MAIN_CONTENT_HEIGHT = height - 56 - 16 - 202;

const styles = StyleSheet.create({
  center: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  centerColHorizontally: {
    alignItems: 'center',
  },
  centerColVertically: {
    justifyContent: 'center',
  },
  centerRowHorizontally: {
    justifyContent: 'center',
  },
  centerRowVertically: {
    alignItems: 'center',
  },
  alignColLeft: {
    alignItems: 'flex-start',
  },
  alignColRight: {
    alignItems: 'flex-end',
  },
  alignColBottom: {
    justifyContent: 'flex-end',
  },
  alignRowLeft: {
    justifyContent: 'flex-start',
  },
  alignRowRight: {
    justifyContent: 'flex-end',
  },
  alignRowBottom: {
    alignItems: 'flex-end',
  },
  wrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  mainContentContainer: {
    height: MAIN_CONTENT_HEIGHT,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
  },
  col: {
    flex: 1,
    flexDirection: 'column',
  },
});

export default styles;

export default {
  USER_ID: 'user_id',
  AUTH_TOKEN: 'auth_token',
  REFRESH_TOKEN: 'refresh_token',
  REFRESH_TOKEN_TIMELINE: 'refresh_token_timeline',
};

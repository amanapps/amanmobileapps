import { DefaultTheme, configureFonts, Colors } from 'react-native-paper';

const fontConfig = {
  ios: {
    regular: {
      fontFamily: 'Poppins-Regular',
      fontWeight: 'normal',
    },
    medium: {
      fontFamily: 'Poppins-Medium',
      fontWeight: 'normal',
    },
    light: {
      fontFamily: 'Poppins-Light',
      fontWeight: 'normal',
    },
    thin: {
      fontFamily: 'Poppins-Thin',
      fontWeight: 'normal',
    },
  },
  android: {
    regular: {
      fontFamily: 'Poppins-Regular',
      fontWeight: 'normal',
    },
    medium: {
      fontFamily: 'Poppins-Medium',
      fontWeight: 'normal',
    },
    light: {
      fontFamily: 'Poppins-Light',
      fontWeight: 'normal',
    },
    thin: {
      fontFamily: 'Poppins-Thin',
      fontWeight: 'normal',
    },
  },
};

export const regularFont = {
  fonts: { regular: 'Poppins-Regular', medium: 'Poppins-Regular' },
};

export const mediumFont = {
  fonts: { regular: 'Poppins-Medium', medium: 'Poppins-Medium' },
};

export const boldFont = {
  fonts: { regular: 'Poppins-Bold', medium: 'Poppins-Bold' },
};

export const colors = {
  colorPrimary: '#194743',
  colorOnPrimary: '#fff',
  colorBackground: '#fff',
  ...Colors,
};

export const paperTheme = {
  ...DefaultTheme,
  // roundness: 16,
  fonts: configureFonts(fontConfig),
  colors: {
    ...DefaultTheme.colors,
    primary: colors.colorPrimary,
    onSurface: colors.colorOnPrimary,
    text: '#338e86',
    background: colors.colorBackground,
  },
};

/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
// import codePush from 'react-native-code-push';
import { Provider as StoreProvider } from 'react-redux';
import { Provider as PaperProvider } from 'react-native-paper';
import { NavigationContainer } from '@react-navigation/native';
import { SafeAreaProvider } from 'react-native-safe-area-context';

import store from '@redux';
import Routes from '@route';
import { paperTheme } from '@config/theme';
import RootLoader from '@components/RootLoader';
import ModalRoot from '@components/ModalRoot';
import { UpdateTokenInHeader } from './src/api/AuthApi';

function App() {
  return (
    <StoreProvider store={store}>
      <SafeAreaProvider>
        <PaperProvider theme={paperTheme}>
          <NavigationContainer>
            <Routes />
          </NavigationContainer>
          <ModalRoot />
          <RootLoader />
          <UpdateTokenInHeader />
        </PaperProvider>
      </SafeAreaProvider>
    </StoreProvider>
  );
}

// let codePushOptions = {
//   checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
// };

// export default codePush(codePushOptions)(App);
export default App;
